var express = require('express');
var app = express();
var db = require('./util/db');
var bodyParser = require('body-parser');
global.__root = __dirname + '/';

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,UPDATE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, Accept,Origin, X-Requested-With');
  next();
});

// Api starting point
app.get('/api', function (req, res) {
  res.status(200).send('API works.');
});

app.use(express.static(__root + "uploads"));

var JobController = require(__root + 'jobseeker/jobseekerController');
app.use('/api/jobs', JobController);

var RecruiterController = require(__root + 'recruiter/recruiterController');
app.use('/api/recruiter', RecruiterController);

var AuthController = require(__root + 'user/authController');
app.use('/api/auth', AuthController);

var VaccancyController = require(__root + 'vaccancy/vaccancyController');
app.use('/api/vaccancy', VaccancyController);

module.exports = app;