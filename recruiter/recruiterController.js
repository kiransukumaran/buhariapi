var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

// var VerifyToken = require('./VerifyToken');

router.use(bodyParser.urlencoded({
    extended: false
}));
router.use(bodyParser.json())

var Recruiter = require('../Schemas/schema').recruiterModel;
var Jobs = require('../Schemas/schema').jobsModel;
var Address = require('../Schemas/schema').addressModel;

router.post('/', function (req, res) {

    var jobs = new Array();
    req.body.recruitment.forEach(elem => {
        jobs.push(new Jobs(elem));
    })

    Recruiter.create({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        companyName: req.body.companyName,
        country: req.body.country,
        contactNo: req.body.contactNo,
        whatsappNo: req.body.whatsappNo,
        companyNo: req.body.companyNo,
        companyEmail: req.body.companyEmail,
        companyAddress: new Address({
            address: req.body.companyAddress.address,
            city: req.body.companyAddress.city,
            state: req.body.companyAddress.state,
            country: req.body.companyAddress.country,
            zipcode: req.body.companyAddress.zipcode
        }),
        recruitment: jobs
    }, function (err, recruiter) {
        if (err) return res.status(500).send("There was a problem submitting the details.");
        res.status(200).send({
            auth: true,
            id: recruiter._id,
        });
    })
})

// RETURNS ALL THE RECRUITER IN THE DATABASE
router.get("/", function (req, res) {
    Recruiter.find({ status: 1 }, function (err, recruiter) {
        if (err) return res.status(500).send("There was a problem finding the details.");
        res.status(200).send(recruiter);
    })
})

// RETURNS ALL THE RECRUITER WITH STATUS 0
router.get("/status", function (req, res) {
    Recruiter.find({ status: 0 }, function (err, recruiter) {
        if (err) return res.status(500).send("There was a problem finding the details.");
        res.status(200).send(recruiter);
    })
})

// GETS A SINGLE RECRUITER FROM THE DATABASE
router.get('/:id', function (req, res) {
    Recruiter.findById(req.params.id, function (err, recruiter) {
        if (err) return res.status(500).send("There was a problem finding the user.");
        if (!recruiter) return res.status(404).send("No user found.");
        res.status(200).send(recruiter);
    });
});

// UPDATES A SINGLE RECRUITER IN THE DATABASE
router.put('/:id', function (req, res) {
    Recruiter.findByIdAndUpdate(req.params.id, req.body, {
        new: true
    }, function (err, recruiter) {
        if (err) return res.status(500).send("There was a problem updating the user.");
        res.status(200).send(recruiter);
    });
});

module.exports = router;


