var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var Vaccancy = require('../Schemas/schema').vaccancyModel;

router.post('/',function(req,res){
    Vaccancy.create({
        slNo: Date.now(),
        category: (req.body.category).toUpperCase(),
        country: req.body.country,
        company: req.body.company,
        salary: req.body.salary,
        description: req.body.description,
        status: 1
    }, (err, vaccancy) => {
    
        if (err) return res.status(500).send("There was a problem registering the vaccancies.");
        res.status(200).send({
            auth: true,
            id: vaccancy._id,
        });
    });
})

router.get("/", function (req, res) {
    Vaccancy.find({}, function (err, vaccancy) {
        if (err) if (err) return res.status(500).send("There was a problem finding the details.");
        res.status(200).send(vaccancy);
    })
})

router.get("/hide", function (req, res) {
    Vaccancy.find({ status: 0 }, function (err, vaccancy) {
        if (err) if (err) return res.status(500).send("There was a problem finding the details.");
        res.status(200).send(vaccancy);
    })
})

router.get("/show", function (req, res) {
    Vaccancy.find({ status: 1 }, function (err, vaccancy) {
        if (err) if (err) return res.status(500).send("There was a problem finding the details.");
        res.status(200).send(vaccancy);
    })
})

router.get('/:id', function (req, res) {
    Vaccancy.findById(req.params.id, function (err, vaccancy) {
        if (err) return res.status(500).send("There was a problem finding the details.");
        if (!vaccancy) return res.status(404).send("No user found.");
        res.status(200).send(vaccancy);
    });
});

router.put('/:id', function (req, res) {
    Vaccancy.findByIdAndUpdate(req.params.id, req.body, {
        new: true
    }, function (err, vaccancy) {
        if (err) return res.status(500).send("There was a problem updating the details.");
        res.status(200).send(vaccancy);
    });
});

module.exports = router;