var express = require('express')
var router = express.Router()
var bodyParser = require('body-parser')
var multer = require('multer')
const FormData = require('form-data')
var fs = require('fs')

let fonts = {
  Roboto: {
    normal: 'node_modules/roboto-font/fonts/Roboto/roboto-regular-webfont.ttf',
    bold: 'node_modules/roboto-font/fonts/Roboto/roboto-bold-webfont.ttf',
    italics: 'node_modules/roboto-font/fonts/Roboto/roboto-italic-webfont.ttf',
    bolditalics:
      'node_modules/roboto-font/fonts/Roboto/roboto-bolditalic-webfont.ttf'
  }
}

var PdfPrinter = require('pdfmake/src/printer')
var printer = new PdfPrinter(fonts)

router.use(
  bodyParser.urlencoded({
    extended: true
  })
)
router.use(bodyParser.json())

var User = require('../Schemas/schema').jobseekerModel
var Qualification = require('../Schemas/schema').qualificationModel
var Passport = require('../Schemas/schema').passportModel
var Experience = require('../Schemas/schema').experienceModel
var Address = require('../Schemas/schema').addressModel

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})
router.post('/', multer({ storage: storage }).array('uploads[]', 12), function (
  req,
  res
) {
  req.body = JSON.parse(new FormData(req.body).data)

  var bq = new Array()
  req.body.basicQualification.forEach(elem => {
    bq.push(new Qualification(elem))
  })

  var tq = new Array()
  req.body.technicalQualification.forEach(elem => {
    tq.push(new Qualification(elem))
  })
  var experienceDetails = new Array()
  req.body.experienceDetails.forEach(elem => {
    experienceDetails.push(new Experience(elem))
  })
  var gulfRegion = []
  var indiaRegion = []
  var others = []
  var gulfExp = 0
  var otherExp = 0
  var indiaExp = 0
  var gulfExpCount = 0
  var indiaExpCount = 0
  var otherExpCount = 0
  var count = 0

  experienceDetails.forEach(elem => {
    if (elem.type == 'gulf') {
      gulfRegion.push(diff_day(elem.toDate, elem.fromDate))
      count = gulfRegion.reduce((total, value) => {
        return total + value
      })
      gulfExp = count / 30
      if (gulfExp >= 12) {
        gulfExpCount = (gulfExp / 12).toFixed(1) + 'years'
      } else {
        gulfExpCount = gulfExp.toFixed(1) + 'months'
      }
    } else if (elem.type == 'india') {
      indiaRegion.push(diff_day(elem.toDate, elem.fromDate))
      count = indiaRegion.reduce((total, value) => {
        return total + value
      })
      indiaExp = count / 30

      if (indiaExp >= 12) {
        indiaExpCount = (indiaExp / 12).toFixed(1) + 'years'
      } else {
        indiaExpCount = indiaExp.toFixed(1) + 'months'
      }
    } else {
      others.push(diff_day(elem.toDate, elem.fromDate))
      count = others.reduce((total, value) => {
        return total + value
      })
      otherExp = count / 30
      if (otherExp >= 12) {
        otherExpCount = (otherExp / 12).toFixed(1) + 'years'
      } else {
        otherExpCount = otherExp.toFixed(1) + 'months'
      }
    }
  })

  totalExperience = gulfExp + indiaExp + otherExp
  if (totalExperience >= 12) {
    overallTotalExperience = (totalExperience / 12).toFixed(1) + 'years'
  } else {
    overallTotalExperience = totalExperience.toFixed(1) + 'months'
  }
  User.create(
    {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      age: req.body.age,
      DOB: req.body.DOB,
      fatherName: req.body.fatherName,
      motherName: req.body.motherName,
      spouseName: req.body.spouseName,
      contactNo: req.body.contactNo,
      whatsappNo: req.body.whatsappNo,
      landPhone: req.body.landPhone,
      religion: req.body.religion,
      postApplied: req.body.postApplied,
      languagesKnown: req.body.languagesKnown,
      totalExperienceGulf: gulfExpCount,
      totalExperienceIndia: indiaExpCount,
      totalExperienceOthers: otherExpCount,
      overallExperience: overallTotalExperience,
      attachments: req.files,
      basicQualification: bq,
      technicalQualification: tq,
      passportDetails: new Passport({
        passportNo: req.body.passportDetails.passportNo,
        dateofIssue: req.body.passportDetails.dateofIssue,
        dateofExpiry: req.body.passportDetails.dateofExpiry,
        ECR: req.body.passportDetails.ECR
      }),
      experienceDetails: experienceDetails,
      address: new Address({
        address: req.body.address.address,
        city: req.body.address.city,
        state: req.body.address.state,
        country: req.body.address.country,
        zipcode: req.body.address.zipcode
      })
    },
    function (err, user) {
      if (err)
        return res
          .status(500)
          .send('There was a problem submitting the details.')
      res.status(200).send({
        auth: true,
        id: user._id,
        email: user.email,
        status: user.status
      })
    }
  )
})

// RETURNS ALL THE USER IN THE DATABASE
router.get('/', function (req, res) {
  User.find({ status: 1 }, function (err, user) {
    if (err)
      return res.status(500).send('There was a problem finding the details.')
    res.status(200).send(user)
  })
})

// RETURNS ALL THE USER WITH STATUS ZERO
router.get('/status', function (req, res) {
  User.find({ status: 0 }, function (err, user) {
    if (err)
      return res.status(500).send('There was a problem finding the details.')
    res.status(200).send(user)
  })
})

router.get('/image/:id', function (req, res) {
  if (fs.existsSync('resume/' + req.params.id + '.pdf')) {
    let pdf = fs.readFileSync('resume/' + req.params.id + '.pdf')
    res.setHeader('Content-Type', 'application/pdf');
    res.setHeader('Content-Disposition', 'attachment; filename=' + req.params.id + '.pdf');
    res.send(pdf)
  } else {
    User.findById(req.params.id, function (err, row) {
      function getDocument (row) {
        return {
          content: [
            {
              columns: [
                [
                  {
                    text: row.firstName + ' ' + row.lastName,
                    bold: true,
                    fontSize: 20,
                    alignment: 'left'
                  },
                  {
                    text: row.email,
                    fontSize: 10,
                    italics: true
                  },
                  {
                    text: +row.contactNo,
                    fontSize: 10,
                    italics: true
                  }
                ],
                [
                  {
                    image: row.attachments[row.attachments.length - 1].path,
                    fit: [100, 150],
                    alignment: 'right'
                  }
                ]
              ]
            },
            {
              canvas: [
                {
                  type: 'line',
                  x1: 0,
                  y1: 5,
                  x2: 520,
                  y2: 5,
                  lineWidth: 2,
                  lineColor: '#C0C0C0'
                }
              ]
            },
            {
              text: 'Personal Details'.toUpperCase(),
              style: 'header'
            },
            {
              columns: [
                [
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      { text: 'Date of Birth: ', fontSize: 10, bold: true },
                      { text: String(row.DOB).slice(0, 10), fontSize: 10 }
                    ]
                  },

                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      { text: 'Age: ', fontSize: 10, bold: true },
                      { text: row.age, fontSize: 10 }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      { text: "Father's Name: ", fontSize: 10, bold: true },
                      { text: row.fatherName, fontSize: 10 }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      { text: "Mother's Name: ", fontSize: 10, bold: true },
                      { text: row.motherName, fontSize: 10 }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: row.spouseName ? "Spouse's Name: " : '',
                        fontSize: 10,
                        bold: true
                      },
                      {
                        text: row.spouseName ? row.spouseName : '',
                        fontSize: 10
                      }
                    ]
                  }
                ],

                [
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: row.religion ? 'Religion: ' : '',
                        fontSize: 10,
                        bold: true
                      },
                      { text: row.religion ? row.religion : '', fontSize: 10 }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      { text: 'Languages Known: ', fontSize: 10, bold: true },
                      { text: row.languagesKnown, fontSize: 10 }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: row.whatsappNo ? 'Whatsapp Number: ' : '',
                        fontSize: 10,
                        bold: true
                      },
                      {
                        text: row.whatsappNo ? row.whatsappNo : '',
                        fontSize: 10
                      }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: row.landPhone ? 'Landline Number: ' : '',
                        fontSize: 10,
                        bold: true
                      },
                      { text: row.landPhone ? row.landPhone : '', fontSize: 10 }
                    ]
                  }
                ]
              ]
            },
            {
              canvas: [
                {
                  type: 'line',
                  x1: 0,
                  y1: 10,
                  x2: 520,
                  y2: 10,
                  lineWidth: 2,
                  lineColor: '#C0C0C0'
                }
              ]
            },
            {
              text: 'Address Details'.toUpperCase(),
              style: 'header'
            },
            {
              columns: [
                [
                  {
                    text: row.address.address,
                    margin: [15, 0, 0, 0],
                    fontSize: 10
                  },
                  {
                    text: row.address.city + ' ' + row.address.state,
                    margin: [15, 0, 0, 0],
                    fontSize: 10
                  },
                  {
                    text: row.address.country + ' ' + row.address.zipcode,
                    margin: [15, 0, 0, 0],
                    fontSize: 10
                  }
                ]
              ]
            },
            {
              canvas: [
                {
                  type: 'line',
                  x1: 0,
                  y1: 10,
                  x2: 520,
                  y2: 10,
                  lineWidth: 2,
                  lineColor: '#C0C0C0'
                }
              ]
            },
            {
              text: 'Posts Applied'.toUpperCase(),
              style: 'header'
            },
            {
              columns: [
                { margin: [15, 0, 0, 0], fontSize: 10, ul: [, row.postApplied] }
              ]
            },
            {
              canvas: [
                {
                  type: 'line',
                  x1: 0,
                  y1: 18,
                  x2: 520,
                  y2: 18,
                  lineWidth: 2,
                  lineColor: '#C0C0C0'
                }
              ]
            },
            {
              text:
                row.basicQualification.length != 0
                  ? 'Basic Qualification'.toUpperCase()
                  : '',
              style: 'header'
            },
            row.basicQualification.length != 0
              ? getEducationObject(row.basicQualification)
              : '',
            row.basicQualification.length != 0
              ? {
                  canvas: [
                    {
                      type: 'line',
                      x1: 0,
                      y1: 20,
                      x2: 520,
                      y2: 20,
                      lineWidth: 2,
                      lineColor: '#C0C0C0'
                    }
                  ]
                }
              : '',
            {
              text:
                row.technicalQualification.length != 0
                  ? 'Technical Qualification'.toUpperCase()
                  : '',
              style: 'header'
            },
            row.technicalQualification.length != 0
              ? getTechnicalObject(row.technicalQualification)
              : '',
            row.technicalQualification.length != 0
              ? {
                  canvas: [
                    {
                      type: 'line',
                      x1: 0,
                      y1: 20,
                      x2: 520,
                      y2: 20,
                      lineWidth: 2,
                      lineColor: '#C0C0C0'
                    }
                  ]
                }
              : '',
            {
              text: 'Experience Details'.toUpperCase(),
              style: 'header'
            },
            row.experienceDetails.length != 0
              ? getExperienceObject(row.experienceDetails)
              : '',
            {
              columns: [
                [
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: 'Total Experience - Gulf: ',
                        fontSize: 10,
                        bold: true
                      },
                      { text: row.totalExperienceGulf, fontSize: 10 }
                    ]
                  },

                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: 'Total Experience - India:',
                        fontSize: 10,
                        bold: true
                      },
                      { text: row.totalExperienceIndia, fontSize: 10 }
                    ]
                  }
                ],

                [
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: 'Total Experience - Others:',
                        fontSize: 10,
                        bold: true
                      },
                      { text: row.totalExperienceOthers, fontSize: 10 }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: 'Overall Total Experience:',
                        fontSize: 10,
                        bold: true
                      },
                      { text: row.overallExperience, fontSize: 10 }
                    ]
                  }
                ]
              ]
            },
            {
              canvas: [
                {
                  type: 'line',
                  x1: 0,
                  y1: 25,
                  x2: 520,
                  y2: 25,
                  lineWidth: 2,
                  lineColor: '#C0C0C0'
                }
              ]
            },
            {
              text: 'Passport Details'.toUpperCase(),
              style: 'header'
            },
            getPassportObject(row.passportDetails),
            {
              canvas: [
                {
                  type: 'line',
                  x1: 0,
                  y1: 25,
                  x2: 520,
                  y2: 25,
                  lineWidth: 2,
                  lineColor: '#C0C0C0'
                }
              ]
            },
            {
              text: 'Declaration'.toUpperCase(),
              style: 'header'
            },
            {
              text:
                ' I certify that the information I am about to provided is true and complete to the best of my knowledge.',
              fontSize: 10,
              margin: [15, 0, 0, 5]
            },
            {
              columns: [
                [
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      { text: 'Date: ', fontSize: 10, bold: true },
                      {
                        text: String(row.created_at).slice(0, 10),
                        fontSize: 10
                      }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      { text: 'Place:', fontSize: 10, bold: true },
                      { text: row.address.city, fontSize: 10 }
                    ]
                  }
                ],
                [
                  {
                    text: 'Signature \n' + '(' + row.firstName + ')',
                    style: 'sign'
                  }
                ]
              ]
            }
          ],
          styles: {
            header: {
              fontSize: 12,
              bold: true,
              margin: [0, 20, 0, 10],
              decoration: 'underline'
            },
            sign: {
              margin: [15, 0, 0, 5],
              alignment: 'right',
              italics: true
            },
            name: {
              fontSize: 16,
              bold: true
            }
          }
        }
      }
      const documentDefinition = getDocument(row)
      var file = printer.createPdfKitDocument(documentDefinition)
      file
        .pipe(fs.createWriteStream('resume/' + row._id + '.pdf'))
        .on('finish', function () {
          var pdf = fs.readFileSync('resume/' + row._id + '.pdf')
          res.setHeader('Content-Type', 'application/pdf');
          res.setHeader('Content-Disposition', 'attachment; filename=' + row._id + '.pdf');
          res.send(pdf)
        })
      file.end()
    })
  }
})

router.get('/resume/:id', function (req, res) {
  if (fs.existsSync('buhari_resume//' + req.params.id + '.pdf')) {
    let pdf = fs.readFileSync('buhari_resume//' + req.params.id + '.pdf')
    res.setHeader('Content-Type', 'application/pdf');
          res.setHeader('Content-Disposition', 'attachment; filename=' + req.params.id + '.pdf');
    res.send(pdf)
  } else {
    User.findById(req.params.id, function (err, row) {
      function getDocument (row) {
        return {
          content: [
            {
              image:
                'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/4QJ+RXhpZgAATU0AKgAAAAgABVEAAAQAAAArAAAASlEBAAMAAAABAAAAAFECAAEAAAGAAAAA9lEDAAEAAAABfwAAAFEEAAEAAAABfwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA44Yz7a5y6bmIYmBfjo2M0tHQ6ZVJ+OrW8sWV5nYUsbCwGxcU2Y5J+ebK3nIU99q27Ovq53IK2nsm8t3G68qj6GwF9vbm3qhz9vv15nwh99Wq76Nb7Na39v/99vfs9+/n9vPe9fX09c2i+OG97uXY9vvv2ptf/vvm7tOq1se4420P/ff08ePK5p5f8XIHenh36s6x/vHWhYOD7/z38HYS7/bspaSjnpyb8WwC4sGbODQymJiY9dK04qVfSkhG4eDg//vd//HNxMLC29vagICH+/fn////+/fru7q6+/Pb//vz//vv///7+/Pf+/Pj//v3///3+/fj//fv+/vv0II9+/vz+/////fr+//7+/v3///z+v/3+/fv+/vr//vr//v7///v+/v7//fn+v/z//fj+/Pn//Lj+vff//Hb//v////q//Hf//Po+//u//ff+vPs+vv/+vfb//fb+vPg+fHb+fLe+vTj+vPh+fDa+vTk+fLf+fHc+PDZ+O/X+O/Y/////9sAQwACAQECAQECAgICAgICAgMFAwMDAwMGBAQDBQcGBwcHBgcHCAkLCQgICggHBwoNCgoLDAwMDAcJDg8NDA4LDAwM/9sAQwECAgIDAwMGAwMGDAgHCAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgA4wE7AwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A/bzNG6iivhz2A3UbqKKADOaKKB1q0AAZqQDAoQbaM0wDrTlWhVzQzZqvUAY7jSquKFXFLQl1AKAM0qrmngVokS2AGBShc0qpT1XNaRiSIq4pwSnAYoq7E8wAYop6wk+1PEQQf3qqxPMQgZpwjY9qp6L4u0vxBqOoWdhqVjfXWkyCG9hglV3s3IyFcA5ViOcH0pq+LrM+Nv8AhH90g1H7ANSAK4VofNMRwe5DdR2BFQqkLJpqzdvn29SvZ1Ltcruld+nf8UXhCxpfIb2pmt6va+HNFu9Rv7iOzsbGJp7i4lO2OFFGWYn0Ao0rVLfXNLt76zmjurO7jWaCaJg6SowBVlI4IIIOa0uubkvrvbrbuRaXLz202v0v2H+Q3tSGJhU+2jbT5WLmZXKlaSrGKRkDUrBzEFNKVM0HpUZG00rFKRGy00pU1NKVDiXchprJUzLmoyMVnKIyOgjNOZM02s2aXGkbTR98U48im/dNTsA3pRmnEbhTaWwEdFFFZAFFFFVEApyCmr1qQcCqADQBk0U77q00AMcChFxQoyadTXcApyrmkUZNPFaJEtgKcq4pVXAp6ritIxJBUp1FSRxY5NaWM5SGpEX9qS9vbbR7OS4uriC1t4RukmnkEcaD1LHAH41MT8wHc9B61xfx90fwv4o+H8mjeLnkXRdSubeOYocCI+cpjZzztjMgVCxGPnwSM5GeInKnRlUja6Ttd2V+l3rZd3bQ0w1NVa0acr2bV7K7t1stLvsr6jPHHxYl04LZ+H7NNa1TUNNn1DSZFlVrLUGgKmS2Ein/AFpQkqOhAJz8pqbQ/jZoGs6nprf2vY2tlrWmC/sTcOIGnIcrKoLkDfH8oZOqknNeX6r8KLS/+Ln/AAjnw3t28M2WmyjVNZv7WA/YNMvolYW6wRnCNM5YrKiYUx8NgkV6BD8E/DOhfCb7H4+m0fxNBZzz6re6jrFtDBbpPK7PJKBwsS5Y9CBjr1rx8LXzGvWnyRVo3V7+5dNaJ6Se8rvlaTSW8Xze1isNl1GjDnbvKztb37NPVrWK2VlzJtO+0ly+V/Cj4j33gD4pRaTB4j+HNx4VsZZZdbu9G08xpGhWRo3urx22faGc8IhZjljwOs0NnqvhjxHpvjvwNoeveLtFbW79IbNJNsstndQK0rxGUjFv9qjLpu47gc5rw/4h/wDBXD9jL9jPxbqk83xRh8WatF5kMOjeHIG1Syso2bf5MKwILRWVlI3tJvGSC2CBXmviH/g7j/Z707Vvsul+B/i3rMZIVZU0+yhDkkDAVrrd9Bj096nB8N4uVBQxE+VxleNtXG1rJOTa5dL8tmm9dNjoxef4dV3PDw5k42lzaKV73bUbPm1spJppaa7n1t478IeNvj34rSTTvCnjbwDrjQBBq2oa5G2l28XG6JrWPcsobn5SoJJyTgVJ8I/2uP8AhWem32i/FHVZ7fWdJ3QQW9v4YntvMERdfkdQY5A4VSm0KAKb4U/4KPa1qnwu03xjrH7OH7QOg6JqkYljzpNheXkEZx881nDdtdRKOD80WcDJAANbHw8/a6+BfxT+K1nq0fjD+yfEniLThpdrpfiFp9JXUo1Y5WGC6VEuJAWKkw78BsHrWk+G8XRrrFYOs+dt83Mrxaa3cYOCcl0b111eiM4Z/hatB4XGUFyJLl5NJJrpzTU2ovqlppotXfW+JfxyX/hJNM1nw/rq3Oh6N4cvdcvI7V0nt9Rd9kNpC5U8MZWOMEHKkepHp/w9utWu/Bul/wDCQLaR+IltYjqcNq4aOGcqCyjHQZ6ZrM0H9nLwP4XuJ5NL8O6fpsd5ew6hcw2qeXDdSxZMW5RxtVjvCrhdwBwa8vsfijr37JXiLUNP8c6ZNfeCtU1Oe8tPFliDMbQzys4jvU5ZQu4KHHGFA57Htq+Aq/WMwdoT3cbuEHok3dXSaS10jF83M9UxOhQx1P6vl6vOGylZTktW0rO0mm3prKS5bL3Wj3zFIVpdJ1S18RaTb31jc295ZXaCSGeBxJHIp6MpHUc/pUxh3n+6f/1f419IkpLmWqPmndPle5XKYppGamaMqP8AP+e9NIzWbh2ArvD/AHajqyVxTHTfWbXcpSISuajZalZShpCMipZopFcjBprLmpmWoyMGspRKI6CM051ptZM0Gj5TTsUjDIpA+KnYCGiiisQCiilUZNaIBVWnHrQOBRQAqjNK3LUE7VoQVXkA4DFFFOQVQDlGBTkWkUZNSIMmtoozFRe9OoqSKP8AirRGcpCxxbetJd3SWNpLPJv8uFGkfahdsAZOAOSfYcmpQM1T8RrqX9izf2P9g/tIAGEXocwMc8htnzcjuOhx16Veqi5Ex1kkzwnUvitpura1J461C7tNN1bQbCW60nw/q96bdVssMWuvlAKTzw9I5AxTb93kkL4A+GmoftX+PI/G/jrRTZ+DbaMt4b0G+KvNKsixN9omMZxsJjDLG27lieR15+Pwbdftp/Ha3n1vRNO03w94AvHstd+y6kbuHW7+I5jjHC7kQN95lDDc65xXc/tGfGG8klm8J+GftarHPDpl/cWEwhubi7mXdDpVnJ/yyneP95LPj/RrcGQDcysnyWT4CpmdSU67vQjLTS3tZLeU01e0Xoo7Nx2slf7LNMZTy2nGnQVq8o2bvf2UXtGDTteS95y3Slvdu3Aft/f8FJb79mXw3c6N8L/A2pfFj4kPb3r2WlWSSNbh7QIbhcxKzzyxeZGzwRDdjIZ0baG/nM/a+/bM/aa/4KLfESSx8eXXjfXZTMWt/C2nadPDY2ZDAYjso1wWUgDc4Z8jlia/qD8Mfsz+Dfhj8JL658ZWmjXjQWv2zUrzyfKt9MigVnWO0/igggXds2kOSWdmaR3Y8l4H+FEnwx8E6x8VPEXxE1/wJcatClxcPrtzbXttoGmKxNraSy3iNKoQSF5CZh+9lkySAuP0SPKtj4e3c/Ab9jL/AINu/wBpP9q6+s7rWvDi/CvwzMA8mo+KFaC6CEAjZZD9+zHPAcRr1ywr6kt/GH7Af/BHzRPE3grSNT1X4yfGe40660m98WwabHfw6HctG6EQkusNvhiQfIMsq8q0hxiv0D/ax/4KF/CXwN+z54kh8W/tL/CHXtC1fS57Ke00e3+26rfRSxlDHALC/EiyMu4BwFAJySAOPxK/am/by+DGs/8ABNvR/hn8HvBej+Add8T6tHd+K7Wy025+2+TaeYsMdzqE80huVlLpKFTAVkIIXODor7iuft5+01/wXJ+FPg79hnUPi98J/Ffw58fX9usUsXh/VPEaaPfyKWxIn2d1M5mXnEWwbsEg4HP86n/BSL/goB4g/wCCjP7Seq+O9Y06z0HTpJHXStHtUj26fCccPIqIZpWwC8rjcxwOAAB4BRUxilsM+0f+Cff/AAXg+PX7Amo2Nja+ILjx14GtyFk8M+ILh7iBI/S3mOZLcjtsJQHqjdK/oH/YD/4KmfDD/gq78FtUTwPqkOh+MlsHj1Tw3q0STXelOy7fM8o/Lc2+4jDrwRwwRsqP5J67D4G/HfxZ+zN8WNG8beA9e1Dw74m0GZZ7S+tm2OjY+ZGHIeNgSrI2VdSQwwSKmpTjOLjLroVGTi1Jbo/rH0b4beLP2KZdPvNAW+8X+B7zYmu6NaQFptMnIAe7so+T5bNlmiycZ645HbaT8bfFHjDwZ4M8ZaJoEeoaDrSM+oaZA3malEkkoSCVDwu1AC0g7ds4zXj3/BHH/gqz4f8A+Cp37Ox1Ro7PR/iF4ZEdr4p0SJ/ljkZfkuoQSW+zzYbGSSrBkJOATH4y8GeIf2V/E+ueEfDF3dLb+MokuvC18z/6QDbuZZtGWT/lnvVpDEVAO5/U5r4HMsPUyVL6u5fV3dcsbNwno48t024za5eV6Jyts7L7TL6sM4b+sKP1hWfM7pThqpc1rWlFNS5lq1G+6u/r5UyxGR8p4x6cf5/CopIuOmDj/P8AWvCvgN8drqz1nQNN1HUrzX/CvjRHfwzrV2M3ltcICZdNvSAMzJhtrkAttIOTXvfs3TtX0mX4+ljaPtaXzXbRNeTTTTTWjTTPmswy+rgqvsqvqn31afmmmmmnqmmiq6bTg0xlq1JFn/H/AD/n+kDLtPNdUonERMu4YNQOuw1ZZaY6bxWElbQaZARkVGy5qRhtNNcZFQ0axZCRimMMGpXGRTWGRWMolojpCmTS0VmWV6KKKwQBTkHNNqROlWAGlXlqSnLwtNADHJpw4FNTrTqa7gA5NSDimoKkQZNaxRMhyjAqQDApqDmnVskZyHRpvPtUw5pqLtWpFGK1jG5kwIYgqn3iMLkEjPbOOa8n/wCFs+JtOa60vxx4Uu7DT7qV7QeIfDN6bu0tAThWkAxNAwyMuVIB5OBXc/FX4dt8U/Ad3oi6leaUt48Zea2kaNpEVwzRMVwwRx8p2kHB4ryb9imXS/gx4YvPA+rR3Oj+J7jWr2Zrae3mEc6s5ERjmZdjKYlXHzZODXj5hiKyxlLDr3ISTfNdcrenuOLWt91aUXvbY9vL8PQ+pVcQ7TnFpctnzJWfvqSelut4yW19zrJfDll+yd8CINA8Fxi81fUrv7Fo4v381r/UbljiWcrgsigNLIVwRFC+MYrz/wDYRS18aalc3GpTK2ufD9ZfD72s7A3E+olydV1c55cXd0sixyr8pSJ9pG9kXqvjz42m0bx5rmrW+yRvht4WkvLCJvmWbVr8yQWy7f7yrCyAHtee9dbb/s4+E9J+Fug6Xqkaxv4S09YYdaiuGsb202p+9mW5jZXj3Hc7/NtYk7ga+mwuHpYejGhSjyxSSSWyS6WPDrYipXqyr1m5Sk7tvVtvd3Mv9qvxHaiy0PQb4TNpd1JNruuLFD5rNpemoLmZdvUiSb7LEV/iWVh3r+bHTf8AgoL+1R+2x+0P8RPBfwz8a+NP7F+PusapHD4bvtQjazW0umlb7JHJcEx2wEH7rMTpwu3NfWX7XH/BzXN8MvjbrknwRe58eQQ6QdB0/X/GenRobX9/I8s0CW7R+aj7YWUzKGIVdw42n8u/j9+2p8VP2n/HS+I/G3jjXdY1KKVZrfE/2e3smVtyeRDFtjh2tyNijBrrjGysZ7s+9bH/AINRfj/efBbw7r0l/oNp4q1Ow1G/1Pw/K4ZtJMMSvaWpmRmWSe4bKEKNkZxlm5r4F8F/sc/E3x74svtFsfB2rx32k2SajqX2uMWkWl27jKSXEkpVIQ3RQ5DMflALECv3v/4ID/8ABdbWP2yPAd98O/ifpXiDW/iB4KsEuV1zSNLlvjrNiGWIy3McKsyzIzIGYLiTcDw2c/Tn/BRI/Cr49/sU/F7wnqmjeJtJbxfo0lxeTL4N1GzuJ7m3VXt5Xle12lkaKPDOSABjIFVYjmd7H4hfDL/g3K8cfEHw58SNPtfil8Idd+JXhWwgutD8KeHPFFtqV1rD7t06S8q0G2LhSVIZ2AJVQWrrv+CWX/BF3wH+2l+xF+0Vq3i6bxJpfxw+Hlzd6fZaUwNpDoU8NsZYvOh2je0sySxspOEWPgA8nwP/AIKlfsZ/D/8AYH8b6Fp3wrvfjldalavP/aWv+JtDOj6bcKwUQmwkCJI2VZw5bI5A4O5a+1v+DX7wf47+GXh3XPiLo+uXHiDwJ4w1OfRfGPha18MX9/ep9mtZXtrqG6VfIWUy3Cq0ZbJjck5IUB9Rva5+O+s/DrXPD3gvRfEV9pd3a6H4kkuItMvXTEN61uyLOEPcoZEB9NwrJjk3+XHJJIsKtkgDdtzjJAyBnAHpnAr62+Lnw28S/Eb9gb4A+F9F8H+KtQ10+O/Fum6ZBHpxMuotKNLkEMEKZkZxwSDGoG4Bc4avlvxz4N1f4eeMdS0PX9IvtB1rS7h7a8068geC4spVOGjdH+ZSOmG5qSj2j/gmb+3br3/BOv8AbB8LfEjSJJpNPtZxZ69YKTt1TTZSFuISMgFgvzoT0kjQ9sV/Vx8avDen/tWfsxR6p4U1CO6bULODxJ4X1K3bP75VE1vIjDpuB28dmNfxz6T8O9e8T+Eta1zTtJvNQ0PwrFbtq1/b2zGDThPJ5cXnPjCl5G2At1IwMgCv6V/+DXD9pa++O3/BMix8P6rLPNqHwz1afQYXmXmWyO2e2KnuqeY8QP8A0xx2yeXHYOni8PPDVfhmmn6P9ez6M2wuKqYWvDE0vig016rX7u66o95/ZU+I0V98VdNntoo4dJ+Kmjya3JZgfJp+t2biG+Cr0XfkOfVhmvpwJ8uDXy/4Y0vwn+yn+2V4nHiC6kstP1ywGqaBPM7SWul/aJ9t4hwCIQ8wTDtgEDBOcV9RKRIu5eVIyCDkEGvleEnVjhamHryXPCck0nqu7a6KUuacf7sklsfRcVKlLFQxFCL5Jwi02tH2Xm4x5YSf80W+pGwwfWo3j49u36f5/wA8WGG4YqMjbwehOPrmvppRPmioy7TTGXFWZo8n37f5/OoCMisJRuMglTcM+lRVYPBqGRdjVgyosicYNRMMGp2GRUTjis5I2I3HNNqQ8io6xkXEr0UUVzxGA61IvC/hTF608mqAOtObhaRfvUrnJp9AFQYFLQKF5NWgJAMCnoMCmDmpFHNbRMyQDAp8K5b6UypohtT681qjOTHqMmnquTimoKmhXPP4f5/l+NbwWhmSRLj+nv8A56/lUifPu+bjp9a8b/aB/a6j+BHiz+xY/COveJLr+z01KWSydFhgjeYwJvJyRmTC5xj5h+HafAT4sv8AGn4eLrU2kzaHeJeXFjdafNKJZLSaGQoyswAGeM9OM1y4fOMHVxUsFTnepFO6s+lr62t+P5HoVsnxdPDRxtSFqcnZO667aXuvVr80fP8AdeMpNc+OnxK8O3KFTqXxG8Mtal23C4soobHzAg44W4tZVYDO0yjoWArN/wCC/X7QV1+zj/wSi+KmqafPNa6lr1nF4ctZYpPLkQ3sqwyFT1z5LSnjkYyOleQ/GD9rHwhL/wAFBvEHwQ1LVLPwb8UfDPj/AETxH4I1a4HmR6pBqdtaxXFsEbAkw7HfFkbkKshDxbl6T/gsz+z54m/4KB/sf2fwm1bUNB+FPiaLxHZ6n/a2uNNJ4b1ZYo50Kw3kaN5TM0isEnVH+Urhh89fQcuzR5Nz+eL9hH/gnt8Sv+CjfxP1Pwl8MtP0+81TR9NbVbtr68W1ghhEkcXLsD8xaQYHcA+lfpP+xX/waY+O/FL+MdP+OmqQeE7e40yM+HdQ8O6nHfm3vxKMtPAygSR+XvGAyn5jhh1r6k/4N+f+CP3jT/gnJ+018Wtc8ReIPC/iTSpNNg8O2t1pUsoaSU/Z71mKOoAjMckWGDHJ3dAMn9YUOVGO/NUkTKT6Hw5/wR7/AOCHvhH/AIJOv4k1qDxNe+N/GniiBbK41Sa0WzhtLRX3+RDEGcjc4VnZmOSiYCgHP07+2CcfsqfEb/sXb7/0Q9ej15v+2F/yan8Rv+xdvv8A0Q9aKJHU0v2hv2dvBn7Vnwi1fwJ8QNBs/EnhXXEVLuxuQQCVYMjqykMjqwVlZSCCAQaw/wBkP9jL4c/sKfCRfA/ww8OxeG/DwuXvZIhcS3EtzO6qrSySSMzsxCKMk4AUAYAr1KinZCK9xptrc3dvPJbwyT2rM8EjRhnhLDaxU9VJBIOOoOK/If8A4OUP+CSN1+114j8P+PvhZ4fjuvidbaPeXGqWVnCPtHii1tpbONQoUZe5iW4ypPLRqVz8qCv1/fG35uleVfErxNpvhv8AaS8IX2rahYaXY6X4Z1m5nury4SCGINcaagJZiAOfU0mhxep+cv8AwRs/4ImaPP8A8EyPid8OPjV4J8QeDfGnxA1t9O8RyiYRX32W0lgns/Ik+eJog4LBkDK+5xk9vpj/AIJh+HPCfwe8V+HPAfgnSU0rRNN+E+ivcRW7fuFvlurmWcucZe4f7YksjEk/vUPG7n0D9p3/AIKBx/C/wBHqnhHQb3WIb64W1tdXvoHtdOmYhmLQ78POoCffUCPLKQzcivMv+CYHifxR4++LNxfatG8Og6T4dms9IiS28m2iWS6gLiInJfmL5mZmbPUkmvlcXxbgaObU8kjzSrS1dlpFWveT/RX87H0+G4VxtbKamcytGjHRXesne1or/O3lc+l/in+yv4Z+LfxDj8Ra4+qXDDT10yawS68q0vIFl80JKoG5xvwSN2DtHHXPoMNoLSBY1VY441CoijAUAAY/DiuK+Nv7RXh/4GXdlFrf24/bLa5vC1tF5i28EChnkk5GBlkQdyzgd6sfAj466T+0B4VudU0q31SwayufstzZ6jb+Rc2z7VcblyeGR0YHPQ11UK2WUsbPD0XFV5ayS+J2V9fk7peum5w1qOZVMHDEVlJ0Y6Rb+FXdtPmrN+mux1jLTHXctTyLj6GomGDXpSjbU8+LIWG8e9V5VwcirTrh/wDeqKZcn6/5/wAa55IfkVXFRyruX6VMelRkVzTWoyvTHHNSMNrUx+lZM2iQng0xhzUr9abWMkUinRRRXLE0HRjmnGmp0pxqgHJ1oHLUIOKF+9VAOpydabTo6tbiew9OWqVOtRp1qROlbxIHDk1YFQxcuKmHWtEYyJBwKniHA+nP8/8AP196gUbmA/CrURySfX/E10xJPm39vzwJZeI9b8DtFpcupeINYvW0i3il1JrHT7mIDz2ju2HLLuQMqgg5HBrrv2IfDy/Dn4eXXhG41zwfqWpabctdvaaFcmf7Ckp6SsxLMxZW+Zvp2rsP2hfhtB8UvhJqmmSeHdM8UXSoJ7Kwv52ghknH3f3ikMvU8gj0zXyb+xx49n+FH7Qmq2epabb6Vo0E66JKfDGlLcaPHcOQU+0XhYyZUjGWLAEnOBXwGOlTyviWniqkFav7vMtOXRXvok9Y3b5m0vsrd/oGBjUzLh2phYT1oe84vW+ultW1pKySik3f3nsvhv8A4OaP2cfH/gz/AIKO/Aj4ufCvR9U1TxZ4ghhs9MTTLRry5fVtNuDcQ4iUHfmKRDjBBWF88Cv2e/Zl8a658af2ZvBniDxpoem6Rr3iTQ7a81XTLa5W9tIZJYlZkV+joc574Bxk4yaP7SfhnW73wHD4i8K6dHqvjHwPcHWtHsZJPKGoukTxy2hfB2+fC8sQbBCuyNg7cHg/2Df2h/D3jiPWPBWnyXFjJpMaa7pGm30fk3cOk3bF0TYSdyQTeZCHQsmwQ85JA/U0tLn5tJ3RS0v9lXw7Z/tBfEDQ9D1HxJ8P/wC1rTS9ftT4V1R9NU5863mAgG63IzbxZJi3YkwCAa9HvPhh8TPDUTN4f+JUOrCNCY7bxPocE4duiqZrT7OwQDHVHYnJLHIAf8ZT/wAIN8XvAfjDPl2clxL4X1RwudsN6UNu7H2vIbeMen2g9s16fWqIPHbZP2gLOdZJpPg7qUa/etkj1KyaT6SlptuOv+rbOMcZyOJ/az1v40n9mH4gfavDPwvW2/sC8Epi8TXxcL5LZKg2GCQOgJGfUda+mK83/bC/5NT+I3/Yu33/AKIeqAz59W+OlxC0ceg/CezkYYWdte1C5WI/3jH9jj34/u71z6iq9po3x6v7lYbzX/hTp9rICHubPRr6a4h44KJJcBGOf7xx39q9gooA8r1f9nvxF4x02WHxB8VfHDpMCXh0NbXRo0OCv7t4ojcqNp6Gdju+YEELt4v4V/s2+D7T9qvW7trO88RXPg3w7p2mw3/iK+n1q8juJpJpZCJrp5GV/KitSSpBO4E9a+hLuaO2tpJJZFhjjUu7s21UA5JJPAArzf8AZajk1zwNqPi+4WVJvH+qTa9Gsi4ZLRgkNmD/ANukNuxHYs1AHlf/AAVo0+G5/ZjtZpI1aaDW7cRNtyyblkDYPbIqX/gllf3mp/s3wXF5brbWljcS6fYknJmRZHlklzjjLS7MD/njmuQ/4KN+N7z4z+IrX4V+GfJ1C+t72zS8t0yX+3XHzwxlsYTy7dZJX3EfJPG3RSR9OfC34fab8C/hPovh+CaGOx0CySBriUiNZGHLyNzgbnLMecZavznD5XVlxlXzJaU4UYxb6OTd7fJK7/7d7n3+IzOnHhCjl71qTrSkvKKSV/m3ZekuqPnH9rvxz4R8Z/tN6T4a8SQ6bdx6GdOhhhk1BrNpGupy87TMDhreKKGNypH3mXkZr179ie0sbn4NSa7Zx6j/AMVRqt5qUlxf3P2i4vMytGkhbau1THGm1MfKMDJ6n5l8TfELTPH2neIrHxJY2OvfFvVvEsVpB4e1fQy/2W0FwUWO2kVQQrQEM0hfkDjsa+5vB/hbT/BHhqz0fSbWOx03TYlt7a3QkrDGowFBPPHvzXLwq3jc0r5inFxs3olzrmk1FSav70Yxaabuk0mld314oX1PLKOAakndLVvlfKk5OKdvdlKSaaVm02m9LXZUycevT/P+etVnGRVyUZH41WkGD+tfokkfA02QuMr/ACqKQZTj8/8APvipjwaiK4BA6jOP6Vyy2NStIPn9jyKiYYNTzjB/Mfh2qFxXPPYZBOPmpjcipZ/u/jUVYM0iQv8AdptSN0qOspGhTooorjiaD0GKWkj6UtUA5elCdaFPy0J1qgHU5OlNpyfdrSIpbEkdSr92ok6VKv3a2jsZy2JIPv8A4VMnWoYPv/hU6da2juYy3JIvv/gasJ8qf59AKrxdT9DVg/c/E/zrpiJkm3cgHDDvx1r4r/az8A3XwK8PR2H/AAkWn6P8O5NTjez8KaO0kGp+IleRGujJMASXUn5f4cBRheK+1gciuX+LPwrt/ir4eW1a/vNFvrZxLZ6pYhBeWL5AYxMynaWXKkjnB9RXicSZK8xwcoU9KiT5Xe29rp6q6dl7rfK2lzXV0e5w9nCy/GRqVNaba5lvts1o7NXfvJcyTfLZ2Yz4B/EO68f+Are61Dw7qnhWQsyW1jqcoe7ktlACTOPvAsMZDcg+vBPhP7WX7PK+GtUtfEFjNNpun2Oovq+larBdvZv4U1KYn7Qjyp8yabfZIl+8sEzCZlZC3l+OeE9Rb9lL4p6v4o177dqHibRprjQdDguLyZLnxRM7kNeXO9j5dukZQcfIWGRkgkfanwJ+M2n/AB38JXzPZrZ6pps7abrelyMs32O4UfOhYZWRD1VgSGB+orHhPiiGLX1PFe7XjpZvVpaXdoxipOzfKui5leLTOrifhqeDf1vDe9Rl1Wyb1sryk3FXS5n1fK7STR5jpPibxV8U/hNfWN/p/wDwsPw5dh9P1O3VodK8UaFOpy0U0RItpZozsYPG0IOEdFdWVj13wn/bU8F+PLi80nVr5vBnijS7trK60PxFiwvojnMTYkwsglj2yKULAq/qCBxvj79nTxb8BfEZ8V/CWRbuOGNY5vDs7ALLboGxbISQHhUkmNGIeDLCJ/L/ANGbFsfjz4Z/aK8VfatCt7XTPilpdstprvgHxHsgk8QWQbeYNsuAzxsXaGbHyMzLIqLIwH3Ftbnx/Sx9VRTrPEroysrdCDkGvOf2wTn9lP4je3hy+/8ARD1zvw1+EPwo+KukSap4X0m48N3Vu5tr600q7udCvNMnH3oLiC3kj8uRc/dYEEEEblKk4H7V37PMekfs0fEK4t/G3xFjjTw9fN5EmvPcRt+4f5T5iu2D0Iz09OtUSfRCtuFDPtrzWT9m+S5/4+PiH8S5hjBA1dIc8Y/5ZRKfyrj/AIs/DXwB4H+x6beadr3xB8WaqpGl6DqPiC8v3v2HWR45pWiigU4LzMgVB6ttQgEX7Wf7Q/hm6nsPhjZ6015q/ix2i1a30eOS/vbHTF5uMx26u6STgiCPcBzK7g4ibGL+0D+0Z4y8PeDoLLQ9PsPhst/blNNfVfKudSjiXYrXBtoy0NtbxBhlnaR2Zo4khMsiLXM+FPjV4b/Z81XWPCPw30jSfiR8aPEEiza5aeF7WOHSdGdV2xRzyIAlvbwqSqRswkb5mba0havTPgP+ynqGneJR42+JOqJ4l8bXLrP5Sf8AHlpzAHYEGBvaMMwTgIm5iqh2kkkCiP8AYx/Zx/4Vp4b/ALX1S1uhfXcrXcDamVm1S5ldAk2o30gzuvrn+IAlYYhFCuAjZ539vX4zeHdQ8Laj4Jh8S6HBrlm8N7qGi6kXgh1m2A3m0+0cKjsNrDDZyF6Zrtv2hv2ij4d/tPwr4XvrOPxZ9jaR7+5x/Z+gluImuXJwrSMQsa8ksQSNoNfN/wCyx4C8YeJPin4jsY9N8PW9ys1qninRfFkVxqVwxAbfexyEBGaVi5ABAACgEgZr834qzz2tRZPgY8zqtxnJK6WjulZq8tFe70in7srSS+94Zyb2cHm2NlyqklKEW0m9VZu6bUdXay1dvejeLftf7Enw709tMs/GHhfxZrWo+DdUsjFa6FqyLcTaROGAdFnYb9qYddowDnOTxj6IVdoqh4V8Naf4Q0G303S7G107T7UFYLa2iEcUS5J+VQABkkn8a0K+wybLI4HBQw6tdLW17c1tbJt2V9lslokj5POMweNxc8Q72b0va9ul2kru273b1bYj8qcelVpvvVaqtJ91f89hXdI4IELcGoyMSfrUjdaif75+grlkbdCvL9z8v5VC/SrE/Kt/nuagbpXPMoimGUqGppf9WahrnLiRv3qOpG61HWMjUp0UUVyRNB6HilNNjokpgSJ0oQ81DUkJqrgSU5Pu02nJ0rSIpbEidKlX7tRR1Kh+Wto7GctiSD7/AOFTp1qvEf3lTr1rWO5lLcli+/8AUYqwOY/8/Wq8Zw4qxF0/L/D+ldMXYkmByKkXpUMR+QVKh4rphuTI88/aO/Z20/43eDdahtotN0/xNqOmvpcGrzWolkhhZ9zR56hWwRkcjccV5X+z7+zZqHhv4wXWtXmnN4G8G+CWeDStPguRH/bM/lgTahcspwyso4B4A4wAp3fUER3H/eH5/wCeayfHfgPT/iL4N1HQdSSZ9O1WBra5SKVomeNuCAynI49K8bHcOYSvio49R/eR1tspNXceZ2btzO7tvZXukkexguIcVQw0sC5fu5ad3FOyk4q6V+VWV9k3azbZ5D8J/wBvXwt4/m8TXGpKfDWg6HeQ29nq2oTqkGprMWERXOCC2xmxyNvJIrtfjD+zd4A/ad0S3h8YeHNL8QwwjzLK8OY7q1yOHt7mJlliPfdG4PPWvnz40/sAyWHiSTWNNspPE+jtetfXGlKywyRW8MaLBYWyH5f3hRVeUkMEQADrnjfDXxP8cfBr40+JNc8af2tomrR+C7/UX04zIdLjCOsdlHbIjMu1OBzhtxb8fl8PxZmmWz9hnlFtc1udKytdu7aXLZLlSV7u0m3dWf01bhfLcxj7bJaqTtfkbu72Ssk3zXb5m3a2sUlZ6epeJf8AgnX4o0LWItV+Hvxy8beHdWtlSK3n1+0g18xQqciBpT5NxPEOcJczTKCcgdc3PGPwu/aW8W/CbxD4S1jXvgt4kj1rTrjThqMenajo84EsZQSNGJLhCwyThSoOB0rgfCv/AAUT8Z+GfCdjpmuaPZX3ivQ7G/vfEK3CNCVhjiiktZF2DAMnmgHjHpiuo8Lf8FNVvtNuIr/wsl1q7CxjsItH1Nbm3vJ7vhbdpGUCOROdw5xtPpz7WH8RMjq2TqOLts4vR2u07Jq61TX8ya3PKxHAWdU7tU1JX3Uo6q9k1dp2ejT/AJWpbM67xj4L/aa8ewta2Xjj4S/D+2mA3XNh4eu9avohnnY088UOcd2ibHoay/CP/BNyzmhuT45+I3j7xm2psJNUt4r7+x7bVmAwouGtdt1PGoLARS3DxAMcJ0xn+JP+Cjl34B8aXnhzxR4Jfw9qunRSi7d9TW4topjA01qodVG5ZduCeCp45ry/x1+3j8S/if4bmttAWHwtqmk6BLq+uW0FvuuEVJkIaF5N21Wt5FcHBPBrPGeImTUFJRlKc43XKotSut1aXLZrs9dJWvyu1YPgHN67i5RjCErPmck42ezvHmun321jf4lf620jT/h3+yR8NodPs7fwz4B8L2I2w21vFFZW4PH3UUDc546Ak+9cd+0P8WNe8S/AOy8R/C6+h1TT7u43XuoWt1DDLDZLvErxPONiOGUKS4JXngmvmqLRL/43eHdL1bwfd3WseNvDt4upL9jv59SuGtLgLFPHLd3KC3WdchgiDamH44r6m/Zl/Z61j4I+ENS8Larqml+IPCRdzpltNYKtzFHIS0iTkHZJksQeDnk55wOXC51js5qTw1Km4UJw0qQd2pPb3nZdJKSUW17t/i03xGTYLKIQxNWanXjPWnNWTS391XfWLi3JJ+9b4dfln4R/BrxN8efiPc654dm0nTda8P3Ns2o6pcXi6zYao0sTgXYKr5TX0CNuwBtBKnCnO77k+F/w5tfhf4Ns9Jt7m+1BrdT5t5fTGe6u3LFmd3PJJZmOOgzgACtbQPDun+FNKjsdLsbPTbKHPl29rCsMSZ5OFUACrle3w1wrRyqLqOXNVlfml0u97LZN2V3vJq71bPJ4g4mrZm1C3LTjblW7strvdpXdle0U7LRIKKKK+pkfMgx2jNVpeMD/AD6f0qeQ4Wq8p+b6VzyNIETdajfmT/PFPPWoycOT75+lcsjboQTHK/l/j/n6VC/SpZuFH+egFQueK55lEcv+rNQ1LOfl/Goq5y4kbdajqRu9R1jI1KdFFFccTQdGeKJKEPNEnaqAbUkXao6kQ4AoAkp0dNpUOGrRbgyVD81SRmoQcGpkODW8TMcpw1WAear1OhygrVGMiUHFWIjk/X/P9K5P4vazfeHfhL4m1DS5lttS0/Srm5tZWQOI5EiZ1JU8HkdDXh/7L2r/ABR/aO+CNr4ob4lNo9/c3E8CW66Daywjy22jOdrYPU49/avPxOdRoYuGChTlOcouSS5dotJ6ylFXu0ethcnnWwc8dKpGFOMlFt817yTa0jGT2TPqKM8n86kVsV86/sY/tMeKPih408YeC/F8Nlca54NmaNtRsovLgulWUxEMvQNuGRgAEZ4GKs/8FEfjh4n+Avwg0jWPCuoR6ffXWqizlZ7dJgyGGRujAjIKjmpjxRg/7JlnKUnTindW95NPlas3a6fnbzOj/VnGf2pHJ3y+0lazv7ruuZO9r2a12v5H0PC/H0/z/n61Y3AVgeBry5vfBej3F5J515NYwSXDhdu+Ro1LHHbJJ47V5V+198VvE3w08X/C238PaktjB4k8Rppeoq0CSrNE5Tj5gSpxuwV9fpXtY7NKWEwn1yqm4+7orX95pLqur11PHweWVcXilhKbSk+bfb3U2+nZaaHuh6Vm+IvB2j+MrKS31XTNP1O3lQxvHcwLMrKSCRhgeMgHHqBU2tRyHSLn7PJ5Mwhby3A3bGAODj2NfOX/AAT0/aN8UfGP4b+LNa8baxZ3EWi3qwR3Biito4Y1j3OzFQBjkHJ7elGMzbD0sZRy6tFt1lNrRONoJOV7vs1bRorB5XiKuEq5hRaUaLgnq+a820rWXdO+qPTfiP8AsjeBvifqGuXt/pckGo+ItPTS767tZmjlkgRkZVHVQf3aDdjJAx0q58Sv2a/DPxM+G9v4ZuIrjTreylgubS6sHEN1azwjEcqvg5cepBzk15/onxP8c/tMfEKO88E3n/CL/DTS5Cr6zNZrLdeInHB+zpIMJGDkCRhz15+6O1/a08a6z8M/2b/FOu6DdCz1jSbRZred41k24kTdkMCpyuRyO9eX7XKquFxONWH/AHdnzS5UvaRV5Nx6tXvZuyb1Te56Xs8zp4rDYJ1/3l48seZv2bdopS0snZK6V2lo0noc4/7BvhHWvCniLT/EF9r3iW+8VTW81/qt9cqbwmD/AFQRlUKgUEjgcg85rudE/Z68H6D4kOsRaLatqjaWmivcuCzS2iqEEbL905UAE4yRx04rA/Yv+I2t/Fr9nDw74i8RXS3mrakJ3mlSFYVYLPIi4UAAfKo6Vx//AAUc+N/ij4B/BzS9a8K6hHpt9Nqq2srvbpOGjMMrYwwPOVU9Kx9pkuDylZ4qC5OSM9k5Wd2m238Xvttt3u3rqdEaWcYzNXkrrvn55Q+JqN1aL0S29xKyWyWmh71pGiWegWEdrY2tvZ2sI2pDBGI40HoFHAq1nisX4caldax8PdDvL2RZry70+CeeQLtDyPGrMQOwyTxXH/tT/F+b4PfCC+vtNja58Qak66ZolsihpLi9mO2MKOc45bB4+XHevpq+PoYbBvF1NIRjzfK17Jd+iXfQ+bo4KtiMWsLT1nKVvne12+3VvtqelA5orwL/AIJ7/tE6h8dfhBcW3iC6a48VeGrp7PUndAkkwJJjkIAABwCp46oa96kRmRgDhmHUdjUZPm1HMsFTx2H+Gaur7run5p6PzNM2y2tl2MqYLEfFB2dtn2a8mtV5D6K+Pf2SPiF8VP2nB4yW6+I0miN4Z1D7DF5Gi2somyX+ZtwB4C/jnrXSfBX9o/xz4Z/ay1L4S+OLrTvEEn2c3Njq1pai3fb5YlXzEHy4KHHYhh1IOa+cwvGmGxFOhiHSnGnXlyQk1GzldpJpScldppNq3me5i+D8TQqV6CqQlUox55xTldRsm2rxSdlJNpO/kfTUrc/T/P8An61WL5Gc5ry79tb4ka98Kf2b9e8ReGrpLXVtNeBkkaJZlCNMiOCrAg8MavfsseMdW+IX7PXhPXdduheatq1l9quJRGsYYs7bcKoAGFwPwr1ZZxReYvLEnzqHtL9LOXLve97+W3U8uOU1ll6zK65HPkt1uo821trPvv0PQGOB/hUJOE/z2p8pz8tRSt2POM/j3/z9TXXI4iGU/N9OD9ajc805mySfxqMnmuab0GRznkCoycCnSNuf9Kjc4FYM0iMf7tR7hT3NQ1jJ6myVyvRRRXLEoAcGnSHgU2nMcqKoBtSL/qxUdSoMxU0A9TkUU1DTqtASZzUinioUPFSIa1izMmqSBu341Ch4pwO01sjOSOf+N0nl/BXxh/2BL0fnA4r5p/Yi+Evifx7+ybZtovxI1zwnDc3l1GltaWkEkcZ34J34EgJ68MMdsV9PfFPwpN4/+Gev6HazRW1xrFhNZRzSglIjIhXcQOSOe1eQ/BX9nv4qfAb4ZL4W0HxV4Ha0jllmjuLnSriSaNpDuOP3gUgH1Xv+XyGbZfVq5vSxMqUp0o0pxfJLld5SjZaSi9k/LY+wyfMqVLJquGjVjCq6sJLnjzKyjJN/DJbtee5zf7D3jFPhP8bPFHwf1DRdJh1ixL3R1mxV/M1bZtYtMXJJJVwwwcD5hjubn/BW0M37OeiNt3LHr8ZY/wB0fZ5h+pNehfs9fsoWvwb8Zax4s1XWrjxR4y10t9q1KeEQhFY5ZY0BO0Hao+igDA4rtvjf8GtH+P3w3vvDOtrILO8KuksRAltpVOVkQ+oOfqCR0rOjkOPrcNV8rqWjOXOoJ292N7wjJrRvu1ffdvfoqZ/gaPElDNKV5QjyObV/ela05RT1S7J222S0Nz4eziXwDoTL91tNtj/5CWvFv23GFx8TPgdbqczyeL0kVM/MVVRuPr3Htz+FM+Hvwn+OXwc8OW3h7RvFfgXxFo2nx+RZy6xZTpdW8SjCp+7bDY4xk8DjOAK6P4dfs46zcfEy18bfEPxJD4o8RaXG8elWtpbfZtO0gPwxjU8u5H8Tcj8setiamLzDBQwH1edOTcOZy5eWKjKMpWak+bZqNlq7XsebhYYXL8ZLG/WIVIpT5VHm5pOUZRjo4rl3Tldqyva57Pevizm9o2NflX8IfDPjJ/gZqXiPR4Y/EHg7w94hW61rw44YR3YWNGEkoU5eMLwRnAxuwRX6l36y3Om3EMLKs0kbojN0UkYBP04rxb9if9lfUv2XfC2vaXqmr2GsLrF3Hcxm3haNYiE2MCG65wP1rDi/h2vm2YYSMbxhGNXmmmvdbUeW6fxJtWcbNNaM6eE+IqGU4DFSlaU5SpOMGn7yTlzWa2aTupXTT1R3H7O3xz8M/H74eWeseGZIo4IUWCexwqyaawUfumUcDHGCOCMYrJ/bgZYf2SfHxYn/AJBMg69ckAf0rzW//YY1/wCHP7QTeNvhX4g0zwva3QLX2lXcMktvMxOXQKp/1bfexnKt93A6exftC/C/UPjT8Cde8K291a6bf65bLA1w+6SKE71LdOSMKw7HkdK9GnUzTFZRicHjaNqyhKCaa5Kl4tKUdfdvpdSta+9tvOqQyvDZrh8Xgq16LnGTTT5qfvJuMtNbdHG97dz59/Yp+AXijxh+zP4Z1PTvip4s8P2l4k5TTrOC3aC1xPIpC7lJ5Izz3J4Hbj/+Cknwa8QfDv4Jaffat8RPEni21l1ZIRZ38UKxRsYpSJBsUHcACBnjmvrD9lz4R3XwF+B+i+E76+ttQuNJEqme3Qqjh5XccHn+LmuZ/bc/Zo1D9qr4a6foOnataaS9nqAvXluI2cOBG6bQB3+fPPpXz+YcHylwt9WpU5PEeyiuX2krc1ldWcuXe+mx9BgeLoQ4o+s1JxWH9rJ83s435btp3Ueba3mekfCzP/Cr/Df/AGC7X/0UlfOvj7xd4o+Nn7Wf23wf4ds/FeifCYva+XcamtlC2qTId8gYq24xKNuMcMSe4r3+30DWNF+Edvo9jdWUevWelJaQ3UisbdJ1iCCQqOSuRnHWub/ZR+Bzfs8fCKDQ7q8j1TVZrme/1G+VSv2yeRyS5zznbtHPpX1WZYLE4x4bBq8KatOcla94W5Iq6a+L3no1aNup8vl2Ow2EWIxbtOo7whF81rTvzy0a+z7q1TvK/Q+VtC8Q+If2Sf25LfX/ABNoNr4V0H4lyvDc2sF+t3bxFmXL7wqgFZiHOQMLI2OM196K7FR+prx/9s79l4/tV/DC30m1urXS9U0+8W6tLqZC6xjBWRSBz8wPbuBXc/CrRNa8JfC7SdL17ULbVNY0+0W1nvYgypc7cqr4bncRjPvmuPhnLcXlWMxOCacsPJqpCT5dHL44NK1ve95WilZnXxHmWFzPB4bGJqNeK9nOK5tVH4Jpu99Pdd5N3R8A/AHVvif4Z8DfFTXvhtqFvH/Z2tFtTsvsUc80sX74+dEW7pgkqByDkZIwfpT9gmz8C+P/AA9dePtJn1DVfG2oAW+v3eqXHn31vLgExjAVVjbaNpVQCoA7EDa/Yw/ZY1L9mG28UrqOsWWrN4hu47yNraJ08naJNynd97O4VgR/sWa38MP2kbjxz8OvEGm6HpupEPqOi3MDtb3JJPmIAnCofvLgZVunAxXyHD/DuZZbSweJq03Us5KdNtP2bcpNVKd3yp2dpJatPTW59XxBxBluZVMXhqdRU7qLhUSa9olGKdOpZczV1eLasmtdLHTf8FCXx+x7423bRuggA+v2iPFa37GD5/ZR+H57f2ND/WrX7U/wgvvj98EdU8KaffW+lz6o8LGedGdFWORXIIXnnaBV74AfDi7+EPwb8P8Aha+u4NQuNFtjbtcwoVjlG9mBAPI4IH1Br6/6liP9ZJY1x/dewUObT4vaN2te+2u1j5L69h/9XI4Pm/ee3cuWz+H2aV72tvpvc7LPO41Xnbj3P+f55qSWTA9vr/n2quz5PNfQSZ80hGOBUbttXNK7c1HM+TiueTuykR01zzTicCo2bismbRGMcmoqkqOsJGkSvRRRXKigpT90UlFaAFSQnio6kjOBQA77rU4Dcaa/IplxqEel2c11MdsVrG0zkdlUFj+gqttw1eiPJ/gn+0wfiB8b/G3g3Ukhgm0i8lbRpFXaL21jby5B/tMjqckdifSp/wBsD9pFv2dvAVtNp8cVxrmpXCJAkib44IQ6iWZx2Ubgg/2nHpXkdr8OdS179lTwr8SfDqsvjbw3eXniOEovN3FLdSvcQMo6gpkgexH8Vanxc0C6+KX7Inj7x/rVk0Oq+JtOhuNPtM720rT4pUeGJT/eb5pWI6lx6V8C84zFZdVpK/tfZurGdvsOLlb/ABRfuW3s4y11P0KOT5a8yo1Xb2SqKlOF/tqSjf8AwyXvt7XUo6aH0T4/+INn8Pfh5qXiO4PmWtja/aEVetwxwI0X3dmVR7tXFfskfHXUvjT4M1SHxHbRWHizw3qMlhqtoibFiOSYyB/u5U+6E964DTvEcn7Uc/gHwrY6pcada6Lotp4l1q8tIkZo7lVRbWEBwU4fdIQyn7q8elHxRpt1+yR+1doHii81rUNX0H4jA6XrdzdRRI0dwu3ypGESqvHynO3OPM69a9Otnlf6xTx0L/VotQk9OX3re9vf3JOMdrJc+p59DI6H1apgalvrLTnBa83u393a3vxUpb3b5NDr/hx+2RG/x28ReCfFkUelxx6xcWOg6oUMdreiMgG3djx5oyORwdwHXGfQPiX8RtX8JfF34d6JZx2p0/xRe3dtfGSMtIoitzKuw545BzkHivN/ht8LvD3x/wDA/wAStD16zW6tW8bak0bqds1rJ+72yxN/C3HUcEcHIrl/CVr448BftJfDfwT4ukOtabo9zfXGh+IWB3X0DWjr5EnX96mO5zj1GDSo5lj6VKmqzco1KkVGa3V6qThPycbqMtmvddnZsqZZl9WtN0Uozp05OUHtK1JtTg+/NZuG6fvRurpdx4L+K/xG+K/xn+IvhvRtS8L6VB4Kvo7e3a60uSc3KyBiociQbT8uCQO+cdqxfDf/AAUGnb4Q3F7qHh+G68aLrzeGrTTbGYm21O6GPnRzkrGMjPXkrj72Rj/BL4daf8U/2ofjnHqN1rkFsupwIY9P1KazjuFIdWV/LZd33fqMn1rW/aj/AGfE8A+H/h/r3gbw+slj8OdZGpXWlWSFpp4WaNnkUcmRxsGSctg8dK41ic4WCnj8PUdoyqp3bk2va8qkotNL2cE3o3e1mmdzw2TfXYYDEQV5RpNaKKT9kpOLkmm/aTaWqXLe6aPTrXw58YrvTVupvFngizv2Ac6emiySWsZ6+WZjLvPpvC++K868R/tmeK7v4M+NbjTtF03TfiB8N7pF13S591xby2+4q00DAgleN3PQDvkZ9Rsf2sPhzdeHl1P/AITLQ4bdoxI0c1yEuE9VaL7+8HjaATnIrzP9l34fX3xO+K/xT+IOsaXdad4d8fINN0+1u4TFNdWoGwzMh5UMoGAepJ9Ofcx1SpKrRw2VYiUnVUlJ83Pyrkk1Uu2+VqfKla0ZXat28fL6dONKtic1w6iqTi4rl5OZ88VKnbTmThzN3u1ZO/f0b4p/tDDw1+zEvjjQYo9QvNVs7Y6Pb7TJ9pubjasce0cn5mOQOflNR/Bj9pmPx/8Asqr8QdRNtBNY2FxLqUa8JDcQBt6gHnkgEDP8Yrw39jTRdc1r4g2nw91mOX+yvgrqd7dh35W7ldilmv8AwANM/wD3z6VXj8Fal4X/AGmPE/wZt7Wb/hEfG+rW3id2GdlvZKDLcxDsQ8iJH9B71w0uJMwm6OPs+SonR5LPSta6lte3OnTb8rnfV4dwEFVwN1z02q3Pda0b2cd7X5HGolvq0dt4h/aO+KOkXXwd0lv+EZ0/WviVFK9559jI8dg2EaNQvmAnCuN3PUHGK9E8aeJfiL8JvhD438Qa5qXhXUrnR9NkvNLFlYywqrorEiZWc7geOhHf2ryf9vefQ2/aJ+C8fiKSG30KOe8bUHkdoo44T5Q5dcFRkY4Ire8ZXfw3t/2Y/i1F8PdQgvv+JNNJqAivZ7opIY2VOZCcZwfu9855rWnjMRSrY2jOu26StG9RqWlCMrqFrS968r30d+xlUweHq0cFWhRSVV3laneOteUbOd7x91JJW1Vu9zP8R/tM/ErwB+zr4f8AijfXPg/VNJ1RbWW50ZbGW2uSszY2wy+Y25xnoU6ZPOK+oNA1P+2dGtLzyZIPtkKTeU4w0W5Qdp9xnB96+D/C3ge4/Z+8A/DL4tmxn8XeEzplsms6Te5uTorHAF5bK2QmDjgAYJI4DfL9xeD/ABnpvjvw5Za1o91FfabqMQmgnjPyyKcfrkEEHkEEHpXp8HY/EVpzhi5tS5Kb5JPmburuopNbTbtyr4WmnZs87i7AYejCEsJBOPPUXPFcqVmkqbinZOCV7v4lJPWxk/HH4mL8HfhH4g8VNYyX/wDYdm10LdW2mUjoM9hkgk+ma8/+HHij4i+NfD3hnxJp3irwXr9hqcsMmp6fb2OyOzt3wZBHMJSWkjzjDD5iDwOleifFrxxpfw9+HWqatrdvJdaTaxYu4kg88vExVGyndQGyfYGvk/4h+BfA/hjxb4d1v4C+Iivi/UNUgV9H0W/a4tLm2LfvXmiyRFGo67sKM4xmtuJMdWw2JjUjPmhGKbpxm4Tu5aSitql7cvJLTTTVmHD+BpYjDSpyjyzcmlUlDnhZRTcZPeFr83OrvXXRHq+tfGb4ga9+11rnw68O3fhnTrHTtHh1aG4vbCWeQhtgZTtkXuxwe2O9P/aK+MPj/wDZ5+AVrq11deF9S8SXOuQWLSw2Ui2hhlLAfIWLbhjk5P8ASvLfiPceB7r/AIKM+JV8c3lvZ6PB4at443kupLVRNiM7Q0ZBPyljjOMZq5+2Hd+DW/Y10S38EXEd94cXxXaxxtFPLN84eRpAHc7zgnrnA7GvnMRm2JWEzCrGt78JVlH94+ZJSsrQtZJdJX/M+ioZXhnjMvpOj7k40nL92uVtxu7zvdt9Y2/I9C8bfGzx38D/AIveB9D8RXHhzxNp3ji8OnxnT7OSzvLGTC/vNhdw8Y3cnjgHp30f2afj/r3jr4ieOvBnjKHT7XxJ4RvAYvskZjS7tGJCyhSxP909cYcV5hpFnb/sP/tHxT+JI21nwf40/daV4ivQbi+0CXHNrJK2W8og9R2OT0atz9sG5b4BfFzwb8Z9JtWvIY1bRNaggOftsEikwtnoeRgHpwtbwzTF0FPGVKklGjVSqUm+ZxpySi3zbyV7VYvpHmjfRpc8stwlZwwdOEXKtSbhVS5VKpFuVuXaLtelJfzcstmm+48RfGjxFd/F3xnpehrp/wDYPgbw+13fzywl5JNReN5YoFOQAojCsw69uM5rk/Dn7bU2ifsgaD4+8RafDe694iuHs7DTbBTEt3P5roqDJYqMLknn06muq8D+Bbr4d/sueIJNWXd4i1zTL/WtZk/vXU8LuyfRMrGPZK+b7L4aax42/YB+FuvaDZzarfeBdVl1OawjU+ZcxC4cuFUclhhTjrgkjmsc0zTM8O/aUpSc5UqlTltflvOnay6uEG9OrT0d7GmV5bleIjyVYpQjVp0+a9ua0Kl7vopzS16JrVWPpax0b4xappi3lz4i8E6VfSKH/ssaPLPDEcf6tp/NDZ6gsF47A1xnxQ+OvxG8J638K9C+zeG9J17xvNc2uoiSGS6t7SSNlClMOrFSp3Hnv7V2+g/thfDfxL4dXUv+Er0mx3Jvms7uYQ3du3dGiPz7geOAfavE/wBsDxp4f8f+PPgfqeordW3hW+vrqe4e8SS0ZYMxpvccOgIyQOCQR610Z3jKNHAurgsU5Sbp/wDLy906kE5X15dG02rJJvTty5Lg61bHKjjcKoxSqf8ALu1mqc2lbTm1SaTu20tT13xx4/8AGfwH+GPivxT4qn0HxBb6TaLLZW2l2clm7SF9v7wu75X5l5HYGmeBNW8feKbDw5rtr4m8H+INJ1R0k1G3trExR20LqWJhlEjMzocLhxzznGDWN4d8d/B/wH4N8U33h+7g1jSxHCdaigln1H9y7+UpZZC2UG9iQOwPHFeW+K/Afg/wf8RfCmp/A3XyPEWqavEt5pGkagbmwlszkzSTJyI1UZ4YgckAAgVjjMdUoyhVhUU4JK8Y1W5Xc2uaLt+8vbl5XbVNLUrBYCnWjOlOm4TbdpSpJQsoJ8slf93a/NzRvum9D65c1G5yadIcM2PXio6+0kz4sa54ptBOTRWLNEV6KKK5wCiiirQBT0+7TKkX7lMByHimkdQwyp4II60A4NOI3CqAERY02qqqvYKMAfhS+WuzbtXZjG3HGPTFVNZ1ePQNDvb6WOWWOxt5Ll0jGXdUUsQo9SBx71zP7P3xUf42/CHR/FMlmtgdW85xbq2/ylSaSMDd3OEBJ9TWP1qkq0cO37zTkl5JpN/fJfedCwtWVB4lL3E1FvzabS+6L+47SKGODPlxxx7uu1Qufyp8kUdym2SNJADnDKDimI1ef+PP2q/Afww1/VNL1rXFs9R0eCO4ubf7PI77JCAu3AwxGQSB0Bya0r4yhhoc9eahHa7aS79fJMjDYPEYmfs8PBzl2Sbe6XTzaR6NBDHCp8tFj3HJCgDJpxjR3RmRGaM7kLKCVPTI9DgkcetZem+NNJ1XS9LvrfUrNrXWlVrCRpQgu9y7gEDYLErzgc8Va0PxDYeJrH7Vpt7a39uWZPNt5VkTcpwwypIyCMEV0U61OVlFp9f+D+KMKlGcbuSa6P12/Rl+CCONmZI41Zz8xCgFvqe9TKcH+o4NV1baalWQMP6V0xlbY5pRIX8O6bLe/am07T2uv+ezWsZk/wC+sZrSjm5z69R6/wCf614j8b/2xdO+Afxp8OeG9c02aHR9ctjLJrDNiK3cvtXj+JQR8/cBlPTNdZ8YfjP/AMKt1bwTZw2sd2/jHW4dKWUyYjtkYbmk4+8SvTHHeuCnnWBTqxjNXpNKa6pyty39bqz2+5nqyybHNUZSg7VU3B9Go3vb0s7rfy2PRIoo45GZVRHc5ZlXG/6/p1pfKX7R5hjj8wDaHKjcB1xn0rzD9oj9oNfghJ4Ts47Fr6+8WaxFpduWO2KBSy73fvwrcAdT7CvThJscjdxnp6f55/KvQo46jUqzoQd5QtzLtzK6/DU4K2DrU6UK81aM78r78rs/x0HSWsF2waSGGRl4BdAxH0z0pI9Nt4EZY7e3RXG1gsSjcPQ8c0ocHrnr+X+f6U5W54P4V2aXuc3QDbRm28nyo/Jxt2bRtx6Y6UQxLbptSNY1/uou0fpShmA/vVwn7Svxim+Anwd1LxZHZrqC6VLbmS3LbTJG8yRvg+oViR7jmssVjKWGozxFV2jBNt9kld/gbYXC1MTWhh6KvKbSS7ttJfid0yb1wy7lbseRVfTfD9jo00klnY2VnJMcyNBbpGz/AFIAJo0TVl1zRbO+iVlivIEuEVh8yq6hhn3APNU9D8c6P4n1TULHT9VsL280mUQ3sEM6vJav/dcDkfj7jqDVyqU24ttX6f8AA+RnGnKzSvpv/wAH5mjLp9vKxaS3t3ZupeNWJ+pIphsbcxGL7PAY+pUxDbn1x6+9Sb1GDn05/wA/Wms393gkYH+f896p27E6hcQR3CbZo0kXqFdAwz64NRzQRyW4jMcflx42rtBAx04/KvI9A/aut9d8YfFG2/s2dNN+GVtunfI8+8kVZWl2j7oXCAL65J46V1nwJ+JUnxh+D3h7xPNbraza1aC4eFSSsRLMCoJ54xXk4fOMJiZ+zoyu2pPZ7QlyS+6Wnn00PSxOUYvDQ9pWjypOK3W8488fvjr5dTrpm80MpCsrdQRwahjiS1ULGqxKvIVFCgflTmamk12ykeeU5vD+nzX/ANsbT9Pa7XpObZDKOnR8Z7Dv2p91BFct+8jjkxx86hv503VNWt9KsZrm6uIbW1t1LyzTOI441HUsx4A+tVLHxNpuqanPZWuoWVzeWqJLNBFMryRI4yjFQcgMOQe9c3NCL5dLv8f61NlGpJc2tl+C/qxZS0hgDeXDEm8fNtUDd9agsNJs9IMhs7Ozs/OOZPIgWPzD6sVAz+NeW/FH9sTw/wCDdfk8O+H7W98b+LslF0rSF8zy3BwfNkGVjA7jkj0Fd98PdR1rVvBWnXPiKzt9N1q4i8y6tIH3x2rEkiPdk7iq4BOeTmvPo5jhK9eVGi+aUN2ldJ9ubbm8r37o9CtluKw9CNesnGM9k3Ztb35d+X+9a19jZZsmmO1KxwKZnNdUmccUFFBOBRUlFeiiiucAooopx3AKeh4plKpwasB9ZPjnx9o/wy8KXet69fQ6dpdkuZZpM9+iqByzE8BRkmtYHIrwv9rGLxD44+IfgvwTosvh6w/taK81CO81axW9QzQpt8tUYEK3lu5zjPPBGDXnZtjpYTDSq01eWiirXvKTUV1XVrqvVHp5PgY4vFRo1Hyx1cntaMU5Po+ifR+jPZrTUbPxr4TW60+4hvrHVbQtbzRtujmR0IBB9815L/wT3nkb9l7S7eXO7T769tMH+HbO3+NXP2RvgT4q/Z80bUNL1rxFY6xpMkgl0+0to2VbFySZMFhkKTjCjgHJxzUdx+zfrHhP9ntvDHhPXhp/iCPUhrMd9Nnyjcef5zLgDiM/dwQQQOQcmvLpyxdSVHMqlFxnGnUUoXV7twaS1s78rtd7WvY9eccHSjWyynXUoSqU3GdnayU029Lq3Mr2T1va6Ni4+JusaN+1nb+Fr5I08P63oDXOlMvV7mF8zZ99p6eig96wf2qPilqXhnW7HQPCmn+F7vxZqmmz3TtrIj2zWUZG62QNgyPKw4TOPlJ64rjdX/Zy+O3jPxFo+t6l8RPC9rqmgNK1g9vZH9wZVCP0iAOV9QRWX8Sv2J/it8Yba2TxN4+8M6pJYktbTNpxWaAk5+WRUDqOnGSOM4zXj4zMc2nhq1KhhqvNKV4t8iai7Nq/M7O/Mk7OytoezgstyiGJoVq+JpcsY2mlztOSuk7cq5lblclzK7vqetfEa68B/Er9nrSfHXizRY73R9JsYdcgiVT5ls21T5abSP4gEKk7Tjniuu+CHgnwl4P8GR3XgvT7bT9J8QEaqBCTtlMqgg4JOOMDaOB0rwnSf2UfjNovgZfC9t8TNBXw+tq1iLR9PMiiBtwZfmQk5DEcn8RV3wv+zB8aPh1o1nonh/4qaTbaHpqGG1jk03LohJPIKtkgk/xd67cLmOMjiI4ipl8r8qTklT5ufZ2fPrFr0tZd9OTFZfg3h5YalmEbc7cYt1OTk3V1yWUk/W9321+jtN8U6brF7qFta31pcT6U4ivEjlDNauV3BXx907ecHtWRp/xl8M6o2h/ZdZs5v+Ekmnt9MKNn7Y0O7zNnqo2HnoeMZyK8N0f9kz4p+FdZ1e50r4oaXF/wlEv2nW5ZNGUyXEpXYxVeQRtz1K8k8c5q18Gv2N/FXgXxz4VfXPGlnq3hjwLLNPotlDY+TMHkUg7m7L8xJGW6Ae49KnnGazqQh9TlFN2bbhZLmWuk7/BzX0fvWSVjzKmTZTCnOp9cjJpXSSndvld1rBL4+VLVe7dtp2Pb/iT8KvD/AMYPD0ml+INMtdQhaKSKJ5EBktvMXazRt1VsY5HoK+Sh41utLvfhJ4F1y8+0654I+IEuliWRvmntIQgikPPdZlUcn7vtivpz4n+OfGmitJa+E/A7a9cEfLd3eqW9raIT6ru81segUZ9a8l8M/sOX3i/w74o1jxpqlvb/ABA8U3seow3umrmPQ5Im3RiLJ+b5vvH0wMnqeLiTDV8ViIxy6m/aL4204xcU4yUeZpKUnKKUbX5U220t+/hnFUMJh5SzGqvZt+4k1KSk04uXKm3GKjJuV7czSSTe2x+3tCr6h8I5v4o/Gtsg/H/9Qr6IkOJW/wB6vly5/ZR+LHjfxX4bl8YfErSdc0fw/rEOqpCLEpKWibI24UcsuRySBnPNfQ/xBvb6z8D63c6XH5mpw2FxJaIDjMojYpg/72K9bJald4nF42tRlTU+RpS5bvljZ/C2vxPJzqNBYbCYGhWjUlDnTceblXNK6+KMX66C6F8RtK16y1a6t7tWsdFnlt7q6f5YFaIAy4buE6MegKsOxrySy/a08WfEDxnpP/CF/DjVNT8G3V/FbXGv3zfZkmiZtrTRIcHYBltxyD0wM1w/gb4w/Dnx9+xJB4RuPHdh4XvLrSBZ6jJdEi5gumbdMzIcGTfJuJIPIY8isb9k/wCLPxC8Q/HfT9F0vxfcfELwDZxmO/1KXSfslvaqsZCqkjKDuDBcAE59OM15NfiiVerhKVKpZVVFt0nCT5m/hak3JRS+KSi35xtr7GH4ZVCli6tWnd0nJJVVUjHlS+JNJRc2/hi5JeUr6fZE15HbxSOzbY41LM7cBQM5J9v8Pevnj9sL4maL8ZP2DfEmteH70X+l3xgiSQcEMt3GGVh1B47+oPevRNd/an+HnhmwvptR8XaPZtprvFdW0su26jkQ4ZPIP7wn228/SvGPhl8FLj49fszfEW30uFfCuk/ELX31XQYLqI7baBXjIkKDkLIYyQBwBjHGK9PiDMHiqVTLsE41HUp1U0ndp8toveyTfu62u2rPRnm8P4FYWpTzLGqVNU6lJptNJrmvJWtdtK0tL2Sd90fQ8/i6x+HHwntdT1KSSGz0zTYPM2RNJIcRKoRUXJZiTtCjqSK8g+Bv7Svwt1r4xanHD4bvPAfjLxFs3HVrEWMmrKDlcHONzHPBALEdWIrKT4F/tEQrEqfF3QVjiAVQNO+6BwP+WfYCub+Kf7FHxc+NFhb2/if4i+GNWjtZPMheTTTHLC3+y6xhh16ZxwPrXm5lnGbTdOphMHO9P7M4wd+jtNVLxdtE1F9b3Tsell+U5RBTp4zGQtU+1CVRWW6vB07SV9WnJeVmfXXmsGx90r+nT/CuS+Efxn0X43+HrzUtDnkkt7C/n06USrtdJImwSR2DDDDPODXhmj/s7/tBeHdLtbWH4xaW8FqNoNxavMwXHQu0ZZgP9o5rn/hl+z18SI7XWNU8B/GbwvdTa9evcazJaWYaE3asVOMK2Dt68DJ9Qc12z4lx7rU+XBVFGz5k/Z8z0VnH95rZ76bO/TXhhw3l/sKjljabd48rXtOVau6l+70utVrurdbrpPg94eXVPj7+0npoVmF8I4wv3txkt5Dj8z+tdn/wT/vvtn7Ing4cBraKe3YD+EpcSDB9+/41F+zX+z14m+E3iTxlrnirxJY+INc8WGDzJra3MSgxK6hm4HOGAwB/D3rtPgV8K7f4HfCvS/DNrcSXf2IPJNO//LWWRy8hA7LuY4HYY71lkGW4mnUpYmpBw0r3Ttdc9ZTgnZtbXbs9DTiHNMNVpVcNSnz60LNJ2fJRcJtXSe9krrU6az8RWd9q95YRTLJd6esbXMa8+TvBKAn1IGcehB7ipp7gJGzs22NAWY9cAcmvDfi38I/ihoXxP1bxR8NPEOi20PiBYX1PTtViBj8yGPyxIrEHjYBkfLjHcVk6dpX7Sfia0j3eJfhzpcEi7ku7e3M5f0K4VlP8q9CeeVqVSVGphajabs4qLi1fRpuS3Vr3troebTyGhVpxrU8XSSaV1JyUk7K6aUHs72te61PUfDHiXwr+1R8IZphbDU/DurmW1nt7qPaSY3KkMucqQVDDuODwa5D9mjwj8L9djvvEXgXR47G5s/N8O3FwN6zhYiBzknlgFYNwxGM9MVwXg/8AZa+Nnw+tLi30f4oaDZ215eTX86DTsh5pTl3wU/iPOBgDHApnhz9kr4w+AbnVLrQfido9nd+Irk32qZ04qktwSfnUbTjjGcAZyeOBXz6zLHTqUa+Iy+UpxT53y073to4NzbWt7rs99Nff/s3A06dehh8wjGEmnBc1S1r6qaUEm7Ws+6210tfBOC6/Yo8Zt4S8URWLeGPFF87aP4lihCt5zMcW94+MhiMEFsgEnBx0+hvFXiax8EeHb7VtVuY7PT9Oiaa4mfpGo/mewA5JIFfOPjn9l/41fEnwzc6LrvxK8Oajpd6oWaCTTOpHII/d5BHY5B/rJpf7H/xL1/wiug+KPio82k6ekTafDaWvmlZYmDRGVpFBdVIHBJzgelRluMx+DhLB4bB1PZpfu+ZwXK9fdk+fWN9nvrZ3smXmWDy/GTjjMTjKaqN/vOVTfMtPeiuTSVtGttLq12j3f4b/ABM0X4ueFIdb0G8W+0+Zmj3bSjxOv3kdTyrDuD61u15z+zt8GdX+E1t4judf1yDXta8Tal/aF1Nb2wtoAwQICqDgM2MsR1P5n0RjnivrcBWr1MPGeJjyze6/4Zu11ra7te13a58fmFOhTxM4YWXNBbPXt5pN2d1eyva9lewMdxp1NUbRTq60cZXooorAAooooQBRRniiruA5TXF/FHwDdeKfiB8P9Xs0Jbw7qs0ty+4Ltt5LZ1bPc5YIMD1rss09Wz9ayr0I1ock9rp/OLTX4o3w+InRnzw3s18pJxf4Nig4p33xTaAcVuYCg7TTwc00fOKAdhp7AOpyvTQciiqTAkzinB81ErYp4bNaRkRYlDYpwbNQhsUofNaKQiagMVNMD4pQ9VzEuJ8y/F39mLXNc+IfiLVNL+GPwx1FfNgudNubm4lje6G4eaksQIQyH5juOAM8Fj0+kPCulQ+HPDdnZW9hp+mLDEoa1sU2W8LY+YIABxnOCQCe9XA2aK8vL8nw+Dq1K1Hee+ke7fRJ9bat6Jdj1MwzjE4yjToVtobay7JdZNdL6Jat97HH+NP2dvAfxF8XRa5rnhfSdS1aLb/pMsR3Pt+7vAID47bge3pXaRFIUVV2qigKqquAoHQAelR0V3UsPRpylOnBRctW0km33dt/mefVxFarGMKk21HRJttJdknt8ibzV9aDMo9ahore5hyjpXEqMpUFWGCDzkVi+Dvh9ofw8ivE0PSbHSV1Cc3NwtrEIxNIeCxx/LoK2M4ppeolTi5KbSutn1V97GsZTUXBN2e6vo7bXXkOoJxTd1NLU2yeUg1rSrXxBpN1YXkK3FneRNBPE33ZUYFWU+xBIrG+GPwy0f4O+DLfQdBt5LbTbVneOOSZpWBZix+ZiT3/ACFbxfNNrCVOm6iq8q5kmk7apO11fs7K/obRrVFTdFSfK2m1fRtXSdtrpN6+YrNmmlsUhfmm5ociVECcmiims9TcoGahVwM0KvHNIzZqfMAZs0+o6k3URAhCjFLsFFFQAbBRsFFFMBHGKbRRU9QCiiiqAkXp+FB60UUAAOKkIzRRVRAjBxUlFFEQCgHFFFUBIpyKKKK0MwBxUinIooqogFGaKKoA3UbjRRQAbjRmiigAooooAjLZooorMApjn5qKKmRURKKKKkoa5xQgooqeoA9NoopS3AO1NZiGPJ60UUgP/9k=',
              fit: [100, 100],
              alignment: 'center'
            },
            {
              columns: [
                [
                  {
                    text: row.firstName + ' ' + row.lastName,
                    bold: true,
                    fontSize: 20,
                    alignment: 'left'
                  },
                  {
                    text: row.email,
                    fontSize: 10,
                    italics: true
                  },
                  {
                    text: +row.contactNo,
                    fontSize: 10,
                    italics: true
                  }
                ],
                [
                  {
                    image: row.attachments[row.attachments.length - 1].path,
                    fit: [100, 150],
                    alignment: 'right'
                  }
                ]
              ]
            },
            {
              canvas: [
                {
                  type: 'line',
                  x1: 0,
                  y1: 5,
                  x2: 520,
                  y2: 5,
                  lineWidth: 2,
                  lineColor: '#C0C0C0'
                }
              ]
            },
            {
              text: 'Personal Details'.toUpperCase(),
              style: 'header'
            },
            {
              columns: [
                [
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      { text: 'Date of Birth: ', fontSize: 10, bold: true },
                      { text: String(row.DOB).slice(0, 10), fontSize: 10 }
                    ]
                  },

                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      { text: 'Age: ', fontSize: 10, bold: true },
                      { text: row.age, fontSize: 10 }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      { text: "Father's Name: ", fontSize: 10, bold: true },
                      { text: row.fatherName, fontSize: 10 }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      { text: "Mother's Name: ", fontSize: 10, bold: true },
                      { text: row.motherName, fontSize: 10 }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: row.spouseName ? "Spouse's Name: " : '',
                        fontSize: 10,
                        bold: true
                      },
                      {
                        text: row.spouseName ? row.spouseName : '',
                        fontSize: 10
                      }
                    ]
                  }
                ],

                [
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: row.religion ? 'Religion: ' : '',
                        fontSize: 10,
                        bold: true
                      },
                      { text: row.religion ? row.religion : '', fontSize: 10 }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      { text: 'Languages Known: ', fontSize: 10, bold: true },
                      { text: row.languagesKnown, fontSize: 10 }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: row.whatsappNo ? 'Whatsapp Number: ' : '',
                        fontSize: 10,
                        bold: true
                      },
                      {
                        text: row.whatsappNo ? row.whatsappNo : '',
                        fontSize: 10
                      }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: row.landPhone ? 'Landline Number: ' : '',
                        fontSize: 10,
                        bold: true
                      },
                      { text: row.landPhone ? row.landPhone : '', fontSize: 10 }
                    ]
                  }
                ]
              ]
            },
            {
              canvas: [
                {
                  type: 'line',
                  x1: 0,
                  y1: 10,
                  x2: 520,
                  y2: 10,
                  lineWidth: 2,
                  lineColor: '#C0C0C0'
                }
              ]
            },
            {
              text: 'Address Details'.toUpperCase(),
              style: 'header'
            },
            {
              columns: [
                [
                  {
                    text: row.address.address,
                    margin: [15, 0, 0, 0],
                    fontSize: 10
                  },
                  {
                    text: row.address.city + ' ' + row.address.state,
                    margin: [15, 0, 0, 0],
                    fontSize: 10
                  },
                  {
                    text: row.address.country + ' ' + row.address.zipcode,
                    margin: [15, 0, 0, 0],
                    fontSize: 10
                  }
                ]
              ]
            },
            {
              canvas: [
                {
                  type: 'line',
                  x1: 0,
                  y1: 10,
                  x2: 520,
                  y2: 10,
                  lineWidth: 2,
                  lineColor: '#C0C0C0'
                }
              ]
            },
            {
              text: 'Posts Applied'.toUpperCase(),
              style: 'header'
            },
            {
              columns: [
                { margin: [15, 0, 0, 0], fontSize: 10, ul: [, row.postApplied] }
              ]
            },
            {
              canvas: [
                {
                  type: 'line',
                  x1: 0,
                  y1: 18,
                  x2: 520,
                  y2: 18,
                  lineWidth: 2,
                  lineColor: '#C0C0C0'
                }
              ]
            },
            {
              text:
                row.basicQualification.length != 0
                  ? 'Basic Qualification'.toUpperCase()
                  : '',
              style: 'header'
            },
            row.basicQualification.length != 0
              ? getEducationObject(row.basicQualification)
              : '',
            row.basicQualification.length != 0
              ? {
                  canvas: [
                    {
                      type: 'line',
                      x1: 0,
                      y1: 20,
                      x2: 520,
                      y2: 20,
                      lineWidth: 2,
                      lineColor: '#C0C0C0'
                    }
                  ]
                }
              : '',
            {
              text:
                row.technicalQualification.length != 0
                  ? 'Technical Qualification'.toUpperCase()
                  : '',
              style: 'header'
            },
            row.technicalQualification.length != 0
              ? getTechnicalObject(row.technicalQualification)
              : '',
            row.technicalQualification.length != 0
              ? {
                  canvas: [
                    {
                      type: 'line',
                      x1: 0,
                      y1: 20,
                      x2: 520,
                      y2: 20,
                      lineWidth: 2,
                      lineColor: '#C0C0C0'
                    }
                  ]
                }
              : '',
            {
              text: 'Experience Details'.toUpperCase(),
              style: 'header'
            },
            row.experienceDetails.length != 0
              ? getExperienceObject(row.experienceDetails)
              : '',
            {
              columns: [
                [
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: 'Total Experience - Gulf: ',
                        fontSize: 10,
                        bold: true
                      },
                      { text: row.totalExperienceGulf, fontSize: 10 }
                    ]
                  },

                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: 'Total Experience - India:',
                        fontSize: 10,
                        bold: true
                      },
                      { text: row.totalExperienceIndia, fontSize: 10 }
                    ]
                  }
                ],

                [
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: 'Total Experience - Others:',
                        fontSize: 10,
                        bold: true
                      },
                      { text: row.totalExperienceOthers, fontSize: 10 }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      {
                        text: 'Overall Total Experience:',
                        fontSize: 10,
                        bold: true
                      },
                      { text: row.overallExperience, fontSize: 10 }
                    ]
                  }
                ]
              ]
            },
            {
              canvas: [
                {
                  type: 'line',
                  x1: 0,
                  y1: 25,
                  x2: 520,
                  y2: 25,
                  lineWidth: 2,
                  lineColor: '#C0C0C0'
                }
              ]
            },
            {
              text: 'Passport Details'.toUpperCase(),
              style: 'header'
            },
            getPassportObject(row.passportDetails),
            {
              canvas: [
                {
                  type: 'line',
                  x1: 0,
                  y1: 25,
                  x2: 520,
                  y2: 25,
                  lineWidth: 2,
                  lineColor: '#C0C0C0'
                }
              ]
            },
            {
              text: 'Declaration'.toUpperCase(),
              style: 'header'
            },
            {
              text:
                ' I certify that the information I am about to provided is true and complete to the best of my knowledge.',
              fontSize: 10,
              margin: [15, 0, 0, 5]
            },
            {
              columns: [
                [
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      { text: 'Date: ', fontSize: 10, bold: true },
                      {
                        text: String(row.created_at).slice(0, 10),
                        fontSize: 10
                      }
                    ]
                  },
                  {
                    margin: [15, 0, 0, 5],
                    text: [
                      { text: 'Place:', fontSize: 10, bold: true },
                      { text: row.address.city, fontSize: 10 }
                    ]
                  }
                ],
                [
                  {
                    text: 'Signature \n' + '(' + row.firstName + ')',
                    style: 'sign'
                  }
                ]
              ]
            }
          ],
          styles: {
            header: {
              fontSize: 12,
              bold: true,
              margin: [0, 20, 0, 10],
              decoration: 'underline'
            },
            sign: {
              margin: [15, 0, 0, 5],
              alignment: 'right',
              italics: true
            },
            name: {
              fontSize: 16,
              bold: true
            }
          }
        }
      }
      const documentDefinition = getDocument(row)
      var file = printer.createPdfKitDocument(documentDefinition)
      file
        .pipe(fs.createWriteStream('buhari_resume/' + row._id + '.pdf'))
        .on('finish', function () {
          var pdf = fs.readFileSync('buhari_resume/' + row._id + '.pdf')
          res.setHeader('Content-Type', 'application/pdf');
          res.setHeader('Content-Disposition', 'attachment; filename=' + row._id + '.pdf');
          res.send(pdf)
        })
      file.end()
    })
  }
})

router.get('/attachments/:id', function (req, res) {
  if (fs.existsSync('attachments/' + req.params.id + '.zip')) {
    let file = fs.readFileSync('attachments/' + req.params.id + '.zip')
    res.setHeader('Content-type', 'application/zip');
    res.setHeader('Content-Disposition', 'attachment; filename=' + req.params.id + '.zip');
    res.send(file)
  } else {
    User.findById(req.params.id, (err, user) => {
      if (err) {
        return res.status(500).send('There was a problem finding the details.')
      } else {
        async function attachmentGeneration () {
          var filesArray = []

          await user.attachments.forEach(element => {
            filesArray.push(element.path)
          })
          var zip = require('file-zip')
          zip.zipFile(
            filesArray,
            'attachments/' + req.params.id + '.zip',
            err => {
              if (err) console.log(err)
              console.log('success')
            }
          )
        }
        attachmentGeneration().then(() => {
          if (fs.existsSync('attachments/' + req.params.id + '.zip')) {
            let file = fs.readFileSync('attachments/' + req.params.id + '.zip')
            res.setHeader('Content-type', 'application/zip');
            res.setHeader('Content-Disposition', 'attachment; filename=' + req.params.id + '.zip');
            res.send(file)
          } else {
            setTimeout(() => {
              let file = fs.readFileSync(
                'attachments/' + req.params.id + '.zip'
              )
              res.setHeader('Content-type', 'application/zip');
              res.setHeader('Content-Disposition', 'attachment; filename=' + req.params.id + '.zip');
              res.send(file)
            }, 1000)
          }
        })
      }
    })
  }
})

// GETS A SINGLE USER FROM THE DATABASE
router.get('/:id', function (req, res) {
  User.findById(req.params.id, function (err, user) {
    if (err)
      return res.status(500).send('There was a problem finding the user.')
    if (!user) return res.status(404).send('No user found.')
    res.status(200).send(user)
  })
})

// UPDATES A SINGLE USER IN THE DATABASE
router.put('/:id', function (req, res) {
  User.findByIdAndUpdate(
    req.params.id,
    req.body,
    {
      new: true
    },
    function (err, user) {
      if (err)
        return res.status(500).send('There was a problem updating the user.')
      res.status(200).send(user)
    }
  )
})
function diff_day (fromDate, toDate) {
  var time = (fromDate.getTime() - toDate.getTime()) / 1000
  var days = Math.abs(Math.round(time / (3600 * 24)))
  return days
}

function getEducationObject (educations) {
  return {
    columns: [
      {
        margin: [15, 0, 0, 0],
        fontSize: 10,
        ul: [
          ...educations.map(ed => {
            return [
              ed.type,
              ed.nameofInstitution,
              String(ed.yearofPassing).slice(0, 4) +
                ', ' +
                ed.board +
                ', ' +
                ed.percentage +
                '%',
              '\n'
            ]
          })
        ]
      }
    ]
  }
}

function getTechnicalObject (educations) {
  return {
    columns: [
      {
        margin: [15, 0, 0, 0],
        fontSize: 10,
        pageBreak: 'after',
        ul: [
          ...educations.map(ed => {
            return [
              ed.type,
              ed.stream,
              ed.nameofInstitution,
              String(ed.yearofPassing).slice(0, 4) +
                ', ' +
                ed.board +
                ', ' +
                ed.percentage +
                '%',
              '\n'
            ]
          })
        ]
      }
    ]
  }
}

function getExperienceObject (experience) {
  return {
    columns: [
      {
        margin: [15, 0, 0, 0],
        fontSize: 10,
        ul: [
          ...experience.map(ed => {
            return [
              ed.type,
              ed.companyName,
              String(ed.fromDate).slice(0, 10) +
                ' to ' +
                String(ed.toDate).slice(0, 10),
              ed.position,
              '\n'
            ]
          })
        ]
      }
    ]
  }
}

function getPassportObject (passport) {
  return {
    columns: [
      [
        {
          margin: [15, 0, 0, 5],
          text: [
            { text: 'Passport Number: ', fontSize: 10, bold: true },
            { text: passport.passportNo, fontSize: 10 }
          ]
        },

        {
          margin: [15, 0, 0, 5],
          text: [
            { text: 'Date of Issue: ', fontSize: 10, bold: true },
            { text: passport.dateofIssue, fontSize: 10 }
          ]
        }
      ],

      [
        {
          margin: [15, 0, 0, 5],
          text: [
            { text: 'Date of Expiry: ', fontSize: 10, bold: true },
            { text: passport.dateofExpiry, fontSize: 10 }
          ]
        },
        {
          margin: [15, 0, 0, 5],
          text: [
            { text: 'ECR: ', fontSize: 10, bold: true },
            { text: passport.ECR, fontSize: 10 }
          ]
        }
      ]
    ]
  }
}
module.exports = router
