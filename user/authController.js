var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var User = require('../Schemas/schema').userModel;
var VerifyToken = require('./verifyToken');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('./config'); 

router.post('/', function (req, res) {
    User.findOne({ email: req.body.email }, function (err, user) {
        if (user) return res.json({ status: 401, data: 'Email already exists.' });
        else {
            var hashedPassword = bcrypt.hashSync(req.body.password, 8);
            User.create({
                slNo: Date.now(),
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                password: hashedPassword,
                email: req.body.email,
                role: req.body.role,
                status: 0
            }, (err, user) => {
        
                if (err) return res.status(500).send("There was a problem registering the user`.");
                // if user is registered without errors
                // create a token
                var token = jwt.sign({ id: user._id }, config.secret, {
                    expiresIn: 86400 // expires in 24 hours
                });
                res.status(200).send({ auth: true, token: token });
            });
        }
    })
})

router.post('/login', function (req, res) {
    User.findOne({ email: req.body.email }, function (err, user) {

        if (err) return res.json({ status: 500, data: 'Error on the server.' });
        if (!user) return res.json({ status: 404, data: 'No user found.' });
        // check if the password is valid
        var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
        if (!passwordIsValid) return res.json({ status: 401, data: { auth: false, token: null } });
        // if user is found and password is valid
        // create a token
        var token = jwt.sign({ id: user._id }, config.secret, {
            expiresIn: 86400 // expires in 24 hours
        });
        var value = { firstname: user.firstname, lastname: user.lastname, email: user.email, userid: user._id, role: user.role, status: user.status };

        res.json({ status: 200, data: { auth: true, token: token, value: value } });
    });
});

router.get('/logout', function (req, res) {

    res.status(200).send({ auth: false, token: null });

});

router.get('/', function (req, res) {
    User.find({}, function (err, auth) {

        if (err) return res.status(500).send("There was a problem finding the users.");
        res.status(200).send(auth);

    });
});

router.get('/:id', function (req, res) {

    User.findById(req.params.id, function (err, register) {

        if (err) return res.status(500).send("There was a problem finding the user.");
        if (!register) return res.status(404).send("No user found.");
        res.status(200).send(register);

    });
});

router.put('/:id', function (req, res) {

    User.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, register) {

        if (err) return res.status(500).send("There was a problem updating the user.");
        res.status(200).send(register);

    });
});

router.get('/admin', function (req, res) {

    User.find({ role: 'admin' }, function (err, auth) {

        if (err) return res.status(500).send("There was a problem finding the users.");
        res.status(200).send(auth);

    });
});

router.get('/staff', function (req, res) {

    User.find({ role: 'staff' }, function (err, auth) {

        if (err) return res.status(500).send("There was a problem finding the users.");
        res.status(200).send(auth);

    });
});


module.exports = router;