var mongoose = require('mongoose');

//Qualification Schema
var QualificationSchema = new mongoose.Schema({

    type: {
        type: String
    },
	stream: {
		type: String
	},
    percentage: {
        type: String
    },
    nameofInstitution: {
        type: String
    },
    board: {
        type: String
    },
    yearofPassing: {
        type: String
    }
})

const qualificationModel = mongoose.model("Qualification", QualificationSchema);
const qualificationSchema = QualificationSchema;

//Passport Schema
var PassportSchema = new mongoose.Schema({

    passportNo: {
        type: String
    },
    dateofIssue: {
        type: Date
    },
    dateofExpiry: {
        type: Date
    },
    ECR: {
        type: String
    }
})

const passportModel = mongoose.model("Passport", PassportSchema);
const passportSchema = PassportSchema;

//Experience Schema 
var ExperienceSchema = new mongoose.Schema({

    type: {
        type: String
    },
    companyName: {
        type: String
    },
    fromDate: {
        type: Date
    },
    toDate: {
        type: Date
    },
    position: {
        type: String
    }
})

const experienceModel = mongoose.model("Experience", ExperienceSchema);
const experienceSchema = ExperienceSchema;

//Address schema
var AddressSchema = new mongoose.Schema({

    address: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: true
    },
    zipcode: {
        type: Number,
        required: true
    }
})

const addressModel = mongoose.model("Address", AddressSchema);
const addressSchema = AddressSchema;

//JobSeeker's Schema
var JobSeekerSchema = new mongoose.Schema({

    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    DOB: {
        type: Date,
        required: true
    },
    fatherName: {
        type: String,
        required: true
    },
    motherName: {
        type: String,
        required: true
    },
    spouseName: {
        type: String,
    },
    contactNo: {
        type: Number,
        required: true
    },
    whatsappNo: {
        type: Number
    },
    landPhone: {
        type: Number
    },
    religion: {
        type: String,
    },
    postApplied: {
        type: String,
        required: true
    },
    languagesKnown: {
        type: String
    },
    attachments: {
        type: Array
    },
    totalExperienceGulf: {
        type: String
    },
    totalExperienceIndia: {
        type: String
    },
    totalExperienceOthers: {
        type: String
    },
    overallExperience: {
        type: String
    },
    status: {
        type: Number,
        default: 1
    },
    created_at: {
        type: Date,
        default: new Date().toJSON().slice(0,10)
    },
    basicQualification: [qualificationSchema],
    technicalQualification: [qualificationSchema],
    passportDetails: passportSchema,
    experienceDetails: [experienceSchema],
    address: addressSchema,
})

const jobseekerModel = mongoose.model("Jobseeker", JobSeekerSchema);
const jobseekerSchema = JobSeekerSchema;

//Recruitment Schema
var JobsSchema = new mongoose.Schema({

    recruitFor: {
        type: String
    },
    numberofCandidates: {
        type: Number
    }
})

const jobsModel = mongoose.model("recruitment", JobsSchema);
const jobsSchema = JobsSchema

var RecruiterSchema = new mongoose.Schema({

    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    companyName: {
        type: String, 
		required: true
    },
    companyEmail: {
		 type: String, 
		 required: true
	},
    contactNo: {
        type: Number,
        required: true
    },
    whatsappNo: {
        type: Number,
    },
    companyNo: {
        type: Number, 
		required: true
    },
    status: {
        type: Number,
        default: 1
    },
    recruitment: [jobsSchema],
    companyAddress: addressSchema
})

const recruiterModel = mongoose.model("Recruiter", RecruiterSchema);
const recruiterSchema = RecruiterSchema;

var UserSchema = new mongoose.Schema({
    slNo: {
        type: Number,
    },
    firstname: {
        type: String,
        required: true
    },

    lastname: {
        type: String,
        required: true
    },

    password: {
        type: String,
        required: true
    },

    email: {
        type: String,
        required: true
    },

    role: {
        type : String,
        required: true
    },
    status: {
        type : Number
    }
})

const userModel = mongoose.model('User',UserSchema)
const userSchema = UserSchema

var VaccancySchema  =  new mongoose.Schema({
    slNo: {
        type: Number,
    },
    category: {
        required: true,
        type: String,
    },
    country: {
        type: String,
    },
    company: {
        type: String,
    },
    salary: {
        type: Number,
    },
    description: {
        type: String,
    },
    status: {
        type: Number,
    },
})

const vaccancyModel = mongoose.model('Vaccancy',VaccancySchema)
const vaccancySchema = VaccancySchema

module.exports = {
    qualificationModel: qualificationModel,
    passportModel: passportModel,
    experienceModel: experienceModel,
    addressModel: addressModel,
    jobseekerModel: jobseekerModel,
    jobsModel: jobsModel,
    recruiterModel: recruiterModel,
    userModel: userModel,
    vaccancyModel: vaccancyModel
}


